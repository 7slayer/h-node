Icons:

icons taken from the H2O Icon Theme 0.0.5 (http://kde-look.org/content/show.php/ACUN+Simgeleri?content=83018), licensed under the GNU GPL license:
	
img/devices/small/unknown.png
img/devices/small/soundcard.png
img/devices/small/wifi.png
img/devices/small/printer.png
img/devices/small/scanner.png
img/devices/small/webcam.png
img/devices/small/bluetooth.png

img/devices/big/unknown.png
img/devices/big/soundcard.png
img/devices/big/wifi.png
img/devices/big/printer.png
img/devices/big/scanner.png
img/devices/big/webcam.png
img/devices/big/bluetooth.png

icons taken from the Crystal Projects icons (http://www.everaldo.com/crystal/?action=downloads), licensed under the LGPL license

img/devices/small/videocard.png
img/devices/small/3G-card.png
img/devices/small/acquisition-card.png.png

img/devices/big/videocard.png
img/devices/big/3G-card.png
img/devices/big/acquisition-card.png.png



Fingerprint icons:

The fingerprint icons (listed below) are a derived work from a wikipedia fingerprint icon (http://en.wikipedia.org/wiki/File:Fingerprint_picture.svg) licensed under Creative Commons Attribution-Share Alike 3.0 Unported (http://creativecommons.org/licenses/by-sa/3.0/deed.en)
img/devices/small/fingerprint-reader.png
img/devices/big/fingerprint-reader.png



Libraries:

odict.py
Copyright (C) 2005 Nicola Larosa, Michael Foord
python library to manage ordered dictionaries
licensed under the terms of the BSD license
E-mail: nico AT tekNico DOT net, fuzzyman AT voidspace DOT org DOT uk
Documentation at http://www.voidspace.org.uk/python/odict.html

