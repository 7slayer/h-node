
== Introduction ==

h-client, a client for an h-source server (such as http://www.h-node.com)


== Requirements ==

python

make sure you have the following python modules installed:

	pycurl
	urllib
	htmlentitydefs
	xml.dom
	pygtk

	
== Use the client ==


Extract the tarball inside a folder of your filesystem.

Move to the just extracted folder.

Type the following command:

python hclient.py



h-client, a client for an h-source server (such as http://www.h-node.com)
Copyright (C) 2011  Antonio Gallo


h-client is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

h-client is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with h-client.  If not, see <http://www.gnu.org/licenses/>.

