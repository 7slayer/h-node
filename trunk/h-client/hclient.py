# -*- coding: utf-8 -*-
# h-client, a client for an h-source server (such as http://www.h-node.com)
# Copyright (C) 2011  Antonio Gallo
#
#
# h-client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# h-client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with h-client.  If not, see <http://www.gnu.org/licenses/>.

import pygtk
pygtk.require('2.0')
import gtk

from hlibrary import *

class hclient:

	#the device that has to be displaced in the right window
	currentDevice = None
	currentDeviceCode = None

	#get the active text from a combo
	#combo: the gtk combo instance
	#default: default value if the active index < 0
	def getActive(self, combo, default = 'not-specified'):
		model = combo.get_model()
		active = combo.get_active()
		if active < 0:
			return default
		else:
			return model[active][0]

	#update the current device object by taking the values from the entries
	def applyChanges(self,widget):
		self.currentDevice.setModel(self.modelNameEntry.get_text())

		otherNamesBuffer = self.otherNamesText.get_buffer()
		startiter, enditer = otherNamesBuffer.get_bounds()
		self.currentDevice.setOtherNames(otherNamesBuffer.get_text(startiter, enditer))

		self.currentDevice.setSubtype(self.getActive(self.subtypeCombo))

		self.currentDevice.setYear(self.getActive(self.commYearCombo))

		self.currentDevice.setInterface(self.getActive(self.interfaceCombo))

		self.currentDevice.setDistributions([])
		self.currentDevice.addDistributions(self.distributionEntry.get_text())

		self.currentDevice.setKernel(self.kernelEntry.get_text())

		self.currentDevice.setHowItWorks(self.getActive(self.howItWorksCombo))

		self.currentDevice.setDriver(self.driverEntry.get_text())

		descriptionBuffer = self.descriptionText.get_buffer()
		startiter, enditer = descriptionBuffer.get_bounds()
		self.currentDevice.setDescription(descriptionBuffer.get_text(startiter, enditer))


	#reset the modifications
	def resetChanges(self,widget):
		self.setEntries()
		
		
	#set the device that has to be displaced in the right window
	#and fill the entries
	def setCurrentDevice(self, selection):
		model, path = selection.get_selected()
		if path:
			#get the code:
			code = model.get_value(path, 1)

			if code in self.client.devices:
				
				#set the current device
				self.currentDevice = self.client.devices[code][0]
				self.currentDeviceCode = code
				#get the device
			
				#device = self.client.devices[code][0]

				self.setEntries()

				#make sensitive the apply button
				if self.currentDevice.getType() != 'unknown':
					self.enableButtons()
				else:
					self.disableButtons()

				self.setDeviceInfoLabel()

			else:
				#make non sensitive the apply button
				self.disableButtons()
				self.currentDevice = None
				self.currentDeviceCode = None

				#self.updateStatus()

	#enable the bottom buttons
	def enableButtons(self):
		self.applyButton.set_sensitive(True)
		self.resetButton.set_sensitive(True)
		self.submitButton.set_sensitive(True)

	#disable the bottom buttons
	def disableButtons(self):
		self.applyButton.set_sensitive(False)
		self.resetButton.set_sensitive(False)
		self.submitButton.set_sensitive(False)

	#set the top label
	def setDeviceInfoLabel(self):
		if self.currentDeviceCode != None:
			self.bigIcon.set_from_file("img/devices/big/"+self.currentDevice.getIcon())
			if self.client.devices[self.currentDeviceCode][2] == 'insert':
				self.deviceInfoLabel.set_markup("this device is not present in the server database,\n would you like to <b>insert</b> it?")
			else:
				self.deviceInfoLabel.set_markup("this device is already present in the server database,\n would you like to <b>update</b> it?")

	#set the pyGTK device entries
	def setEntries(self):
		if self.currentDevice != None:
			#set the type entry
			self.setTypeEntry()
			#set the model entry
			self.setModelEntry()
			#set the other names entry
			self.setOtherNamesEntry()
			#set the vendorid:productid entry
			self.setVendorIdProductIDCode()
			#set the subtype entry (in the case of printers)
			self.setSubtypeEntry()
			#set the commercialization year entry
			self.setCommYearEntry()
			#set the interface entry
			self.setInterfaceEntry()
			#set the distribution entry
			self.setDistributionEntry()
			#set the kernel entry
			self.setKernelEntry()
			#set the howItWorks entry
			self.setHowItWorksEntry()
			#set the driver entry
			self.setDriverEntry()
			#set the description entry
			self.setDescriptionEntry()


	#set the subtype entry (in the case pf printers)
	def setTypeEntry(self):
		if self.currentDevice.getType() in self.client.getTypes():
			index = self.client.getTypes().index(self.currentDevice.getType())
		else:
			index = 0

		self.typeCombo.set_active(index)
		
	#set the model name entry
	def setModelEntry(self):
		self.modelNameEntry.set_text(self.currentDevice.getModel())

	#set the other names entry
	def setOtherNamesEntry(self):
		textbuffer = gtk.TextBuffer(table=None)
		textbuffer.set_text(self.currentDevice.getOtherNames())
		self.otherNamesText.set_buffer(textbuffer)

	#set the vendorid:productid entry
	def setVendorIdProductIDCode(self):
		self.vendorIdProductIdEntry.set_text(self.currentDevice.getVendorId() + ':' + self.currentDevice.getProductId())


	#set the subtype entry (in the case pf printers)
	def setSubtypeEntry(self):
		self.subtypeCombo.get_model().clear()
		for subtype in self.currentDevice.getSubtypes():
			self.subtypeCombo.append_text(subtype)

		if self.currentDevice.getSubtype() in self.currentDevice.getSubtypes():
			index = self.currentDevice.getSubtypes().index(self.currentDevice.getSubtype())
		else:
			index = 0

		self.subtypeCombo.set_active(index)


	#set the year of commercialization
	def setCommYearEntry(self):
		self.commYearCombo.get_model().clear()
		for year in self.currentDevice.getYears():
			self.commYearCombo.append_text(year)
			
		if self.currentDevice.getYear() in self.currentDevice.getYears():
			index = self.currentDevice.getYears().index(self.currentDevice.getYear())
		else:
			index = 0
			
		self.commYearCombo.set_active(index)


	#set the interface
	def setInterfaceEntry(self):
		self.interfaceCombo.get_model().clear()
		for interface in self.currentDevice.getInterfaces():
			self.interfaceCombo.append_text(interface)
		
		if self.currentDevice.getInterface() in self.currentDevice.getInterfaces():
			index = self.currentDevice.getInterfaces().index(self.currentDevice.getInterface())
		else:
			index = 0

		self.interfaceCombo.set_active(index)


	#set the distribution entry
	def setDistributionEntry(self):
		self.distributionEntry.set_text(self.currentDevice.createDistroEntry())


	#set the kernel libre entry
	def setKernelEntry(self):
		self.kernelEntry.set_text(self.currentDevice.getKernel())


	#set the howItWorks entry
	def setHowItWorksEntry(self):
		self.howItWorksCombo.get_model().clear()
		for option in self.currentDevice.getHowItWorksOptions():
			self.howItWorksCombo.append_text(option)

		if self.currentDevice.getHowItWorks() in self.currentDevice.getHowItWorksOptions():
			index = self.currentDevice.getHowItWorksOptions().index(self.currentDevice.getHowItWorks())
		else:
			index = 0

		self.howItWorksCombo.set_active(index)

	#set the driver entry
	def setDriverEntry(self):
		self.driverEntry.set_text(self.currentDevice.getDriver())


	#set the description entry
	def setDescriptionEntry(self):
		textbuffer = gtk.TextBuffer(table=None)
		textbuffer.set_text(self.currentDevice.getDescription())
		self.descriptionText.set_buffer(textbuffer)


	#set the node
	def setNode(self,widget):
		self.client.logout()
		self.client.errors = []
		self.client.setNode(self.serverEntry.get_text())
		self.updateStatus()
		self.synchronize(None)
		self.prefWindow.destroy()

	#close the preferences window
	def closePref(self,widget):
		self.prefWindow.destroy()

	#login to the server
	def login(self,widget):
		self.client.login(self.usernameEntry.get_text(),self.passwordEntry.get_text())
		self.updateStatus()

		if self._submitFlag:
			if self.client.isLogged():
				self.applyChanges(None)
				if self.client.submit(self.currentDeviceCode):
					self.synchronize(None)
				else:
					self.printErrors()

		self._submitFlag = False
			
		#self.printErrors()
		self.loginWindow.destroy()

	#submit data to the server
	def submit(self,widget):
		self.applyChanges(None)
		self.licenseNoticeWindow.destroy()
		
		if self.client.isLogged():
			if self.client.submit(self.currentDeviceCode):
				self.synchronize(None)
			else:
				self.printErrors()
		else:
			self._submitFlag = True
			self.openLoginWindow(None)

		

	#logout to the server
	def logout(self,widget):
		self.client.logout()
		self.updateStatus()
		#self.printErrors()
		
	#close the login window
	def closeLoginWindow(self,widget):
		self._submitFlag = False
		self.updateStatus()
		self.loginWindow.destroy()

	#close the license notice window
	def closeLicenseNoticeWindow(self,widget):
		self.licenseNoticeWindow.destroy()

	#open the dialog with the software info
	def openInfoWindow(self,widget):
		about = gtk.AboutDialog()
		about.set_program_name("h-node client")
		#about.set_version("")
		about.set_copyright("(c) Antonio Gallo")
		about.set_comments("simple client for h-node.com, licensed under the GPLv3")
		about.set_website("http://www.h-node.com")
		#about.set_logo(gtk.gdk.pixbuf_new_from_file("battery.png"))
		about.run()
		about.destroy()

	#close the window containing the list of allowed distribusions
	def closeDistroHelperWindow(self,widget):
		self.distroHelperWindow.destroy()
		
	#update the distribution entry
	def setDistributions(self,widget):
		self.currentDevice.setDistributions(self._tempDistributions)
		self.distributionEntry.set_text(self.currentDevice.createDistroEntry())
		self.distroHelperWindow.destroy()

	#add a distrocode to the self._tempDistributions property
	def addTempDistribution(self,widget,data):
		if widget.get_active():
			self._tempDistributions.append(data)
		else:
			try:
				del self._tempDistributions[self._tempDistributions.index(data)]
			except:
				pass
			
		#print self._tempDistributions


	#window containing the list of allowed distribusions
	def openDistroHelperWindow(self,widget,data,a = None,b = None):
		
		#used to temporarily save the list of distributions from the distribution entry or from the distribution checkButtons
		self._tempDistributions = []
		
		self.distroHelperWindow = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.distroHelperWindow.set_title("choose the distribution")
		self.distroHelperWindow.set_position(gtk.WIN_POS_CENTER)
		self.distroHelperWindow.set_icon_from_file("img/icon.png")
		self.distroHelperWindow.set_size_request(300, -1)
		self.distroHelperWindow.set_transient_for(self.window)
		self.distroHelperWindow.set_modal(True)

		self.vboxCh = gtk.VBox(False, 0)
		self.vboxCh.set_border_width(10)
		self.distroHelperWindow.add(self.vboxCh)

		#fill the self._tempDistributions list with the distros already contained inside the distribution entry
		checkedDistros = self.distributionEntry.get_text().split(',')
		
		for distro in checkedDistros:
			if distro != '' and self.client.distroIsAllowed(distro.lstrip().rstrip()):
				self._tempDistributions.append(distro.lstrip().rstrip())

		#create and pack the checkButtons
		for distroCode,distroLabel in self.client.allowedDistros.iteritems():
			chbutton = gtk.CheckButton(distroLabel)
			if distroCode in self._tempDistributions:
				chbutton.set_active(True)
			chbutton.connect("clicked", self.addTempDistribution,distroCode)
			self.vboxCh.pack_start(chbutton, True, True, 2)

		hbox = gtk.HBox(False, 0)
		hbox.set_border_width(10)
		applyButton = gtk.Button(stock=gtk.STOCK_APPLY)
		closeButton = gtk.Button(stock=gtk.STOCK_CANCEL)
		applyButton.connect("clicked", self.setDistributions)
		closeButton.connect("clicked", self.closeDistroHelperWindow)
		hbox.pack_end(applyButton, False, True, 0)
		hbox.pack_end(closeButton, False, True, 3)
		self.vboxCh.pack_start(hbox, False, True, 0)
		
		self.distroHelperWindow.show_all()

	#start the window containing the license notice
	def openLicenseNoticeWindow(self,widget):
		result = self.client.getLicenseNotice();

		if result:
			#window for preferences
			self.licenseNoticeWindow = gtk.Window(gtk.WINDOW_TOPLEVEL)
			self.licenseNoticeWindow.set_title("license notice")
			self.licenseNoticeWindow.set_position(gtk.WIN_POS_CENTER)
			self.licenseNoticeWindow.set_icon_from_file("img/icon.png")
			self.licenseNoticeWindow.set_size_request(300, -1)
			self.licenseNoticeWindow.set_transient_for(self.window)
			self.licenseNoticeWindow.set_modal(True)

			vbox = gtk.VBox(False, 0)
			vbox.set_border_width(10)
			self.licenseNoticeWindow.add(vbox)


			#print result
			##if result

			#description input
			sw = gtk.ScrolledWindow()
			#sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
			sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_NEVER)

			noticeText = gtk.TextView()
			#noticeText.set_decorated(False)
			noticeText.set_editable(False)
			#noticeText.modify_base(gtk.STATE_NORMAL, gtk.gdk.Color(50,100,150) )
			noticeText.modify_base(gtk.STATE_NORMAL, gtk.gdk.color_parse("#a3a3a3") )

			noticeText.set_wrap_mode(gtk.WRAP_CHAR)

			textbuffer = gtk.TextBuffer(table=None)
			textbuffer.set_text(result)
			noticeText.set_buffer(textbuffer)

			sw.add(noticeText)
			#sw.show()
			#sw.show_all()

			vbox.pack_start(sw, False, True, 5)

			hbox = gtk.HBox(False, 0)
			hbox.set_border_width(10)
			applyButton = gtk.Button(stock=gtk.STOCK_APPLY)
			closeButton = gtk.Button(stock=gtk.STOCK_CANCEL)
			applyButton.connect("clicked", self.submit)
			closeButton.connect("clicked", self.closeLicenseNoticeWindow)
			hbox.pack_end(applyButton, False, True, 0)
			hbox.pack_end(closeButton, False, True, 3)
			vbox.pack_start(hbox, False, True, 0)

			self.licenseNoticeWindow.show_all()
		else:
			self.printErrors()
		
		
	#start the login window
	def openLoginWindow(self,widget):
		
		#window for preferences
		self.loginWindow = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.loginWindow.set_title("login")
		self.loginWindow.set_position(gtk.WIN_POS_CENTER)
		self.loginWindow.set_icon_from_file("img/icon.png")
		self.loginWindow.set_size_request(300, -1)
		self.loginWindow.set_transient_for(self.window)
		self.loginWindow.set_modal(True)

		self.window.connect("delete_event", self.delete_event_login)
		
		#self.prefWindow.set_size_request(300, -1)

		vbox = gtk.VBox(False, 0)

		self.loginWindow.add(vbox)

		table = gtk.Table(4, 2, True)
		table.set_border_width(10)

		label = gtk.Label("<b>login to the server</b>")
		label.set_use_markup(True)

		vbox.pack_start(label, False, True, 0)
		
		vbox.pack_start(table, False, True, 0)
		
		### username
		usernameLabel = gtk.Label("username:")
		usernameLabel.set_alignment(0.95,0.5)
		#add the label
		table.attach(usernameLabel, 0, 1, 0, 1)

		self.usernameEntry = gtk.Entry()
		#add the input to the table
		table.attach(self.usernameEntry, 1, 2, 0, 1)

		### password
		passwordLabel = gtk.Label("password:")
		passwordLabel.set_alignment(0.95,0.5)
		#add the label
		table.attach(passwordLabel, 0, 1, 1, 2)

		self.passwordEntry = gtk.Entry()
		self.passwordEntry.set_visibility(False)

		#add the input to the table
		table.attach(self.passwordEntry, 1, 2, 1, 2)

		### create new account
		label = gtk.Label("<a href='http://"+self.client.getNode()+"users/add/en'>Create new account</a>")
		label.set_use_markup(True)
		label.set_alignment(0.98,0.5)
		#add the label
		table.attach(label, 0, 2, 2, 3)

		### request new password
		label = gtk.Label("<a href='http://"+self.client.getNode()+"users/forgot/en'>Request new password</a>")
		label.set_use_markup(True)
		label.set_alignment(0.98,0.5)
		#add the label
		table.attach(label, 0, 2, 3, 4)

		
		hbox = gtk.HBox(False, 0)
		hbox.set_border_width(10)
		applyButton = gtk.Button(stock=gtk.STOCK_APPLY)
		closeButton = gtk.Button(stock=gtk.STOCK_CANCEL)
		applyButton.connect("clicked", self.login)
		closeButton.connect("clicked", self.closeLoginWindow)
		hbox.pack_end(applyButton, False, True, 0)
		hbox.pack_end(closeButton, False, True, 3)
		vbox.pack_start(hbox, False, True, 0)
		
		self.loginWindow.show_all()
		
	
	#start the preferences window
	def openPrefWindow(self,widget):
		#window for preferences
		self.prefWindow = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.prefWindow.set_title("preferences")
		self.prefWindow.set_position(gtk.WIN_POS_CENTER)
		self.prefWindow.set_icon_from_file("img/icon.png")
		self.prefWindow.set_size_request(300, -1)
		self.prefWindow.set_transient_for(self.window)
		self.prefWindow.set_modal(True)

		vbox = gtk.VBox(False, 0)

		self.prefWindow.add(vbox)
		
		table = gtk.Table(1, 2, True)
		table.set_border_width(10)
		
		vbox.pack_start(table, False, True, 0)
		
		### server
		serverLabel = gtk.Label("Server URL:")
		#add the label
		table.attach(serverLabel, 0, 1, 0, 1)

		self.serverEntry = gtk.Entry()
		self.serverEntry.set_text(self.client.getNode())
		#add the input to the table
		table.attach(self.serverEntry, 1, 2, 0, 1)

		hbox = gtk.HBox(False, 0)
		hbox.set_border_width(10)
		applyButton = gtk.Button(stock=gtk.STOCK_APPLY)
		closeButton = gtk.Button(stock=gtk.STOCK_CLOSE)
		applyButton.connect("clicked", self.setNode)
		closeButton.connect("clicked", self.closePref)
		hbox.pack_end(applyButton, False, True, 0)
		hbox.pack_end(closeButton, False, True, 3)
		vbox.pack_start(hbox, False, True, 0)
		#applyButton.connect("clicked", self.applyChanges)
		
		self.prefWindow.show_all()

	#synchronize with the server XML database
	def synchronize(self,widget):
		self.client.sync()
		self.printErrors()
		#print self.client.errors
		self.setEntries()
		self.setDeviceInfoLabel()
		self.setDeviceTree()


	def printErrors(self):
		#destroy the error bar HBox
		if hasattr(self, "errorBarHBox"):
			self.errorBarHBox.destroy()
			
		if len(self.client.errors) > 0:

			self.client.errors = list(set(self.client.errors))
			#self.errorBar.set_shadow_type(gtk.SHADOW_ETCHED_IN)
			
			self.errorBarHBox = gtk.HBox(False, 0)
			for error in self.client.errors:
				label = gtk.Label(error)
				self.errorBarHBox.pack_start(label, False, True, 10)
				
			self.errorBar.add_with_viewport(self.errorBarHBox)
			
			self.errorBar.show_all()
			
			self.client.errors = []

	#check if the user is logged
	#hide or show the login/logout buttons
	def updateStatus(self):
		if self.client.isLogged() == True:
			self.loginButton.hide()
			self.logoutButton.show()
			info = self.client.getUserInfo()
			if info != False:
				self.statusLabel.set_markup("<i>hello</i> <b>"+info['username']+"</b>, <i>you are logged in</i>")

			if self.currentDeviceCode != None:
				self.submitButton.set_sensitive(True)
			
		else:
			self.loginButton.show()
			self.logoutButton.hide()
			self.statusLabel.set_markup("<i>you are not logged in</i>")

		self.printErrors()

	#delete event of the login window
	def delete_event_login(self, widget, event, data=None):
		self._submitFlag = False
		return False


	#update the devices' tree
	def setDeviceTree(self):

		#get the current selection
		ts, itera = self.tree.get_selection().get_selected()
		if itera:
			path = ts.get_path(itera)
			
		self.treestore = gtk.TreeStore(str,str,int,gtk.gdk.Pixbuf,int)

		pci = self.treestore.append(None, ["Your PCI Devices","",800,gtk.gdk.pixbuf_new_from_file('img/title_png.png'),4])
		usb = self.treestore.append(None, ["Your USB Devices","",800,gtk.gdk.pixbuf_new_from_file('img/title_png.png'),4])
		for key,dev in self.client.devices.iteritems():

			if key[0] == 'p':
				self.treestore.append(pci, [dev[0].getType(),key,400,gtk.gdk.pixbuf_new_from_file('img/devices/small/'+dev[0].getIcon()),4])

			if key[0] == 'u':
				self.treestore.append(usb, [dev[0].getType(),key,400,gtk.gdk.pixbuf_new_from_file('img/devices/small/'+dev[0].getIcon()),4])

		self.tree.set_model(self.treestore)

		selection = self.tree.get_selection()
		selection.connect('changed', self.setCurrentDevice)

		self.tree.expand_all()

		#select the device on the tree
		if itera:
			self.tree.get_selection().select_path(path)
		else:
			#select the first device
			self.tree.get_selection().select_path(0)
			ts, itera = self.tree.get_selection().get_selected()
			if itera:
				next = ts.iter_nth_child(itera, 0)
				path = ts.get_path(next)
				self.tree.get_selection().select_path(path)


	def delete_event(self, widget, event, data=None):
		self.client.logout()
		os.system('rm -f tmp/cookies.txt')
		os.system('rm -f tmp/temp')
		gtk.main_quit()
		return False

	def __init__(self):

		#does it have to submit after the login?
		self._submitFlag = False

		#start the client object
		self.client = Client('www.sandbox.h-node.com')
		self.client.createDevices()
		
		# Create the main window
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

		self.window.set_title("h-client")

		self.window.set_icon_from_file("img/icon.png")
		self.window.set_size_request(700, -1)
		self.window.set_position(gtk.WIN_POS_CENTER)
		
		self.window.connect("delete_event", self.delete_event)

		#self.window.set_border_width(0)

		vbox = gtk.VBox(False, 0)

		#add the bottom box
		self.window.add(vbox)
		
		self.centerWindow = gtk.HBox(False, 0)
		self.bottomWindow = gtk.HBox(False, 0)
		
		self.errorBar = gtk.ScrolledWindow()
		self.errorBar.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_NEVER)
		self.errorBar.set_border_width(5)
		self.errorBar.set_size_request(-1,50)
		self.bottomWindow.add(self.errorBar)
		self.errorBarHBox = gtk.HBox(False, 0)
		self.errorBar.add_with_viewport(self.errorBarHBox)
		self.errorBar.show_all()
		
		## build the toolbar ##
		toolbar = gtk.Toolbar()
		toolbar.set_tooltips(True)
		#toolbar.set_style(gtk.TOOLBAR_BOTH)

		pref = gtk.ToolButton(gtk.STOCK_PREFERENCES)
		pref.set_tooltip_text('Preferences')
		pref.connect("clicked", self.openPrefWindow)
		sync = gtk.ToolButton(gtk.STOCK_REFRESH)
		sync.set_tooltip_text('Synchronize with the server: this will override the entries of your devices')
		sync.connect("clicked", self.synchronize)
		info = gtk.ToolButton(gtk.STOCK_INFO)
		info.set_tooltip_text('Information')
		info.connect("clicked",self.openInfoWindow);

		toolbar.insert(sync, 0)
		toolbar.insert(pref, 1)
		toolbar.insert(info, 2)
		toolbar.show_all()

		vbox.pack_start(toolbar, True, True, 0)
		

		vbox.pack_start(self.centerWindow, True, True, 0)
		vbox.pack_start(self.bottomWindow, True, True, 0)
		vbox.show()

		
		## build the left window ##
		
		#start the left vertical box
		self.leftWindow = gtk.VBox(False, 0)
		#self.leftWindow.set_border_width(5)

		self.centerWindow.pack_start(self.leftWindow, True, True, 0)

		#treeFrame.add(self.leftWindow)
		#self.centerWindow.pack_start(self.rframe, True, True, 5)

		#self.leftWindow.pack_start(gtk.Label("Your hardware:"), False, True, 5)

		self.tree = gtk.TreeView()
		self.tree.set_headers_visible(False)

		self.devices = gtk.TreeViewColumn("Your PCI and USB devices")

		device_icon = gtk.CellRendererPixbuf()
		self.devices.pack_start(device_icon, True)
		self.devices.add_attribute(device_icon, 'pixbuf', 3)
		#self.devices.set_cell_data_func(device_icon, self.setTreeViewCell)


		device_name = gtk.CellRendererText()
		self.devices.pack_start(device_name, True)
		self.devices.add_attribute(device_name, "text", 0)
		self.devices.add_attribute(device_name, "xpad", 4)
		self.devices.add_attribute(device_name, "weight", 2)
		
		
		self.tree.append_column(self.devices)
		

		treesw = gtk.ScrolledWindow()
		treesw.set_size_request(110,401)
		treesw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
		treesw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		treesw.add(self.tree)

		self.leftWindow.set_border_width(5)
		self.leftWindow.pack_start(treesw, False, True, 0)

		self.leftWindow.show_all()
		
		#treeFrame.add(self.leftWindow)

		## build the right window ##

		#right top
		rthbox = gtk.HBox(False, 0)
		rthbox.set_border_width(5)
		#login button
		self.loginButton = gtk.Button("Login")
		self.loginButton.set_sensitive(True)
		self.loginButton.connect("clicked", self.openLoginWindow)
		rthbox.pack_start(self.loginButton, False, True, 0)

		#login button
		self.logoutButton = gtk.Button("Logout")
		self.logoutButton.set_sensitive(True)
		self.logoutButton.connect("clicked", self.logout)
		rthbox.pack_start(self.logoutButton, False, True, 0)

		#status label
		self.statusLabel = gtk.Label("")
		self.statusLabel.set_use_markup(True)
		rthbox.pack_end(self.statusLabel, False, True, 0)

		#top image
		self.tihbox = gtk.HBox(False, 0)
		self.bigIcon = gtk.Image()
		self.bigIcon.set_from_file("img/devices/big/unknown.png")
		self.tihbox.pack_end(self.bigIcon, False, True, 0)
		
		self.deviceInfoLabel = gtk.Label("")
		self.deviceInfoLabel.set_use_markup(True)
		self.tihbox.pack_start(self.deviceInfoLabel, False, True, 3)

		#create the entries

		notebook = gtk.Notebook()
		notebook.set_tab_pos(gtk.POS_TOP)
		notebook.show()
		label_base = gtk.Label("Base")
		label_adv = gtk.Label("Advanced")

		self.rightTable = gtk.Table(5, 2, False)
		self.rightTableAdvances = gtk.Table(6, 2, False)

		self.rightTable.set_border_width(5)
		self.rightTableAdvances.set_border_width(5)

		self.rightTable.set_row_spacings(3)
		self.rightTableAdvances.set_row_spacings(3)
		
		notebook.append_page(self.rightTable, label_base)
		notebook.append_page(self.rightTableAdvances, label_adv)

		###type entry
		#year of commercialization label
		self.typeLabel = gtk.Label("Select the device category (if not correct):")
		self.typeLabel.set_alignment(0.75,0.5)
		#add the label
		self.rightTable.attach(self.typeLabel, 0, 1, 0, 1)

		self.typeCombo = gtk.combo_box_new_text()

		for dtype in self.client.getTypes():
			self.typeCombo.append_text(dtype)
		
		#add the combo to the table
		self.rightTable.attach(self.typeCombo, 1, 2, 0, 1)
		
		### model
		#model name label
		self.modelNameLabel = gtk.Label("Model name:")
		self.modelNameLabel.set_alignment(0.94,0.5)
		#add the label
		self.rightTable.attach(self.modelNameLabel, 0, 1, 1, 2)

		#model name input
		self.modelNameEntry = gtk.Entry()
		#add the input to the table
		self.rightTable.attach(self.modelNameEntry, 1, 2, 1, 2)


		### other names
		#other names label
		self.otherNamesLabel = gtk.Label("Possible other names of the device:\n( <i>write one name per row</i> )")
		self.otherNamesLabel.set_use_markup(True)
		self.otherNamesLabel.set_alignment(0.83,0.5)
		self.otherNamesLabel.set_justify(gtk.JUSTIFY_RIGHT)
		self.rightTable.attach(self.otherNamesLabel, 0, 1, 2, 3)
		
		#other names text area
		s = gtk.ScrolledWindow()
		s.set_shadow_type(gtk.SHADOW_ETCHED_IN)
		s.set_size_request(-1,50)
		s.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.otherNamesText = gtk.TextView()
		#self.otherNamesText.set_size_request(-1,60)
		self.otherNamesText.set_wrap_mode(gtk.WRAP_CHAR)
		self.otherNamesText.set_left_margin(5)
		self.otherNamesText.set_right_margin(5)

		s.add(self.otherNamesText)
		s.show()
		s.show_all()
		self.rightTable.attach(s, 1, 2, 2, 3)

		
		### vendorid:productid
		#vendorid:productid label
		self.vendorIdProductIdLabel = gtk.Label("VendorID:productID code:")
		self.vendorIdProductIdLabel.set_alignment(0.88,0.5)
		#add the label
		self.rightTableAdvances.attach(self.vendorIdProductIdLabel, 0, 1, 0, 1)

		#vendorid:productid input
		self.vendorIdProductIdEntry = gtk.Entry()
		#set as not editable
		self.vendorIdProductIdEntry.set_editable(False)
		#add the input to the table
		self.rightTableAdvances.attach(self.vendorIdProductIdEntry, 1, 2, 0, 1)


		###subtype
		#subtype label
		self.subtypeLabel = gtk.Label("Subtype:")
		self.subtypeLabel.set_alignment(0.94,0.5)
		#add the label
		self.rightTableAdvances.attach(self.subtypeLabel, 0, 1, 1, 2)

		#subtype input
		self.subtypeCombo = gtk.combo_box_new_text()
		#add the input to the table
		self.rightTableAdvances.attach(self.subtypeCombo, 1, 2, 1, 2)

		
		###year of commercialization
		#year of commercialization label
		self.commYearLabel = gtk.Label("Year of commercialization:")
		self.commYearLabel.set_alignment(0.87,0.5)
		#add the label
		self.rightTableAdvances.attach(self.commYearLabel, 0, 1, 2, 3)

		self.commYearCombo = gtk.combo_box_new_text()

		#add the combo to the table
		self.rightTableAdvances.attach(self.commYearCombo, 1, 2, 2, 3)


		###interface
		#interface label
		self.interfaceLabel = gtk.Label("Interface:")
		self.interfaceLabel.set_alignment(0.94,0.5)
		#add the label
		self.rightTableAdvances.attach(self.interfaceLabel, 0, 1, 3, 4)

		self.interfaceCombo = gtk.combo_box_new_text()

		self.interfaceCombo.append_text('not-specified')

		self.interfaceCombo.set_active(0)

		#add the combo to the table
		self.rightTableAdvances.attach(self.interfaceCombo, 1, 2, 3, 4)


		### distribution
		#distribution label
		self.distributionLabel = gtk.Label("Distribution used: ")
		self.distributionLabel.set_alignment(0.95,0.5)
		#add the label
		self.rightTable.attach(self.distributionLabel, 0, 1, 3, 4)

		#distribution input
		self.distributionEntry = gtk.Entry()
		self.distributionEntry.connect("button-press-event", self.openDistroHelperWindow)
		
		
		#add the input
		self.rightTable.attach(self.distributionEntry, 1, 2, 3, 4)


		### kernel
		#kernel label
		self.kernelLabel = gtk.Label("Kernel libre version:")
		self.kernelLabel.set_alignment(0.92,0.5)
		#add the label
		self.rightTableAdvances.attach(self.kernelLabel, 0, 1, 4, 5)

		#kernel input
		self.kernelEntry = gtk.Entry()
		#add the input
		self.rightTableAdvances.attach(self.kernelEntry, 1, 2, 4, 5)


		###how it works
		#how it works label
		self.howItWorksLabel = gtk.Label("Does it work?")
		self.howItWorksLabel.set_alignment(0.95,0.5)
		#add the label
		self.rightTable.attach(self.howItWorksLabel, 0, 1, 4, 5)

		self.howItWorksCombo = gtk.combo_box_new_text()

		#add the combo to the table
		self.rightTable.attach(self.howItWorksCombo, 1, 2, 4, 5)
		

		### driver
		#driver label
		self.driverLabel = gtk.Label("Free driver used:")
		self.driverLabel.set_alignment(0.94,0.5)
		#add the label
		self.rightTableAdvances.attach(self.driverLabel, 0, 1, 5, 6)

		#driver input
		self.driverEntry = gtk.Entry()
		#add the input
		self.rightTableAdvances.attach(self.driverEntry, 1, 2, 5, 6)

		
		### description
		#description label
		self.descriptionLabel = gtk.Label("Description:")
		self.descriptionLabel.set_alignment(0,0.5)

		#description input
		sw = gtk.ScrolledWindow()
		sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
		sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		
		self.descriptionText = gtk.TextView()
		self.descriptionText.set_wrap_mode(gtk.WRAP_CHAR)
		self.descriptionText.set_left_margin(5)
		self.descriptionText.set_right_margin(5)

		sw.add(self.descriptionText)
		sw.show()
		sw.show_all()
		
		##add the input
		#self.rightTable.attach(sw, 1, 2, 7, 8)

		self.rightTable.show_all()

		#apply and submit buttons
		hboxBelowEntries = gtk.HBox(False, 0)
		#apply button
		self.applyButton = gtk.Button(stock=gtk.STOCK_APPLY)
		self.applyButton.set_sensitive(False)
		self.applyButton.connect("clicked", self.applyChanges)
		self.applyButton.set_tooltip_text('apply your local modifications: no change will be applied to the server')
		#reset button
		self.resetButton = gtk.Button(stock=gtk.STOCK_REVERT_TO_SAVED)
		self.resetButton.set_sensitive(False)
		self.resetButton.connect("clicked", self.resetChanges)
		self.resetButton.set_tooltip_text('restore the entries')
		#submit button
		self.submitButton = gtk.Button("Submit")
		self.submitButton.set_sensitive(False)
		self.submitButton.connect("clicked", self.openLicenseNoticeWindow)
		self.submitButton.set_tooltip_text('submit your modifications to the server')

		#create the device tree
		self.setDeviceTree()
		
		hboxBelowEntries.pack_end(self.applyButton, False, True, 0)
		hboxBelowEntries.pack_end(self.resetButton, False, True, 0)
		hboxBelowEntries.pack_start(self.submitButton, False, True, 0)
		hboxBelowEntries.show_all()

		#lFrame = gtk.Frame()
		#lFrame.add(rhbox)
		#lFrame.set_border_width(5)
		
		#start the left vertical box
		self.rightWindow = gtk.VBox(False, 0)

		self.rightWindow.pack_start(self.tihbox, True, True, 3)
		self.rightWindow.pack_start(notebook, False, True, 3)
		self.rightWindow.pack_start(self.descriptionLabel, False, True, 3)
		self.rightWindow.pack_start(sw, False, True, 0)
		self.rightWindow.pack_start(hboxBelowEntries, False, True, 10)
		#self.rightWindow.show_all()

		rhbox = gtk.HBox(False, 0)
		rhbox.pack_start(self.rightWindow, True, True, 5)


		rvbox = gtk.VBox(False, 0)
		
		rvbox.pack_start(rthbox, True, True, 0)
		rvbox.pack_start(rhbox, True, True, 0)

		self.centerWindow.pack_start(rvbox, True, True, 0)
		
		#self.rframe.add(self.rightWindow)
		#self.rframe.set_border_width(30)
		
		self.centerWindow.show_all()
		self.bottomWindow.show_all()
		self.leftWindow.show()
		self.window.show()

		self.synchronize(None)
		self.updateStatus()
		
		#self.logoutButton.hide()
		
def main():
	gtk.main()

if __name__ == "__main__":
	Client = hclient()
	main()