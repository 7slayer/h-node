# -*- coding: utf-8 -*-
# hlibrary, a python library to manage the database of an h-source node
# Copyright (C) 2011  Antonio Gallo
#
#
# hlibrary is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hlibrary is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlibrary.  If not, see <http://www.gnu.org/licenses/>.

from hlibrary import *

client = Client()
client.setNode('http://h-source/')
client.createDevices()
#client.request.perform('download/all/it')
#print client.request.url
#client.sync()

#for key,dev in client.devices.iteritems():
	#print key
	#print dev[0].getModel()
	#print dev[0].getDistributions()
	#print dev[0].getYear()
	#print dev[3]
	#print dev[0].getHowItWorks()
	#print dev[0].getDescription()
	#print dev[0].getIcon()
	##dev[0].addDistribution('taranis')
	##dev[0].addDistribution('metad')
	##dev[0].addDistribution('gingo')
	##dev[0].addDistribution('taranis')
	#print dev[0].getDistributions()
	##print dev[0].userDistribution()

client.changeType('u_0846:4260','wifi')


for key,dev in client.devices.iteritems():
	print key
	print dev[0].getModel()
	print dev[0].getType()
	print dev[0].getDistributions()
	print dev[0].getYear()
	print dev[3]
	print dev[0].getHowItWorks()
	print dev[0].getDescription()
	print dev[0].getIcon()
	#dev[0].addDistribution('taranis')
	#dev[0].addDistribution('metad')
	#dev[0].addDistribution('gingo')
	#dev[0].addDistribution('taranis')
	print dev[0].getDistributions()
	#print dev[0].userDistribution()
#client.login('','')
##print client.isLogged()
#client.submit()
#client.logout()

#client.devices['p_14e4:4311'][0].setPost()
#print client.devices['p_14e4:4311'][0].getPost()