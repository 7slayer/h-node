#! /usr/bin/php
<?php

//created by Antonio Gallo (tonicucoz@yahoo.com) and P. J. McDermott
// this script is in the Public Domain

include("connect.php");
include("functions.php");

//insert the PCI vendors
$ids_fp = fopen('pci.ids', 'r');

while (($line = fgets($ids_fp)) !== false) {
	if (preg_match('/^([0-9a-f]{4})\s+(.*)\s*$/',
		$line, $matches) == 1) {
		$vendorid = $matches[1];
		$full_name = sanitizeDb(decode_soft($matches[2]));
		$clean_name = sanitizeDb(decode($matches[2]));
// 		echo "$vendorid $full_name $clean_name".PHP_EOL;
		//insert
		$query = "INSERT INTO vendors (bus,vendorid,clean_name,full_name) VALUES ('PCI','$vendorid','$clean_name','$full_name');";
		DB::$instance->query($query);
		//update
		$query = "UPDATE vendors SET clean_name='$clean_name', full_name='$full_name' WHERE vendorid='$vendorid' AND bus='PCI';";
		DB::$instance->query($query);
	}
}

fclose($ids_fp);

//insert the USB vendors
$ids_fp = fopen('usb.ids', 'r');

while (($line = fgets($ids_fp)) !== false) {
	if (preg_match('/^([0-9a-f]{4})\s+(.*)\s*$/',
		$line, $matches) == 1) {
		$vendorid = $matches[1];
		$full_name = sanitizeDb(decode_soft($matches[2]));
		$clean_name = sanitizeDb(decode($matches[2]));
// 		echo "$vendorid $full_name $clean_name".PHP_EOL;
		//insert
		$query = "INSERT INTO vendors (bus,vendorid,clean_name,full_name) VALUES ('USB','$vendorid','$clean_name','$full_name');";
		DB::$instance->query($query);
		//update
		$query = "UPDATE vendors SET clean_name='$clean_name', full_name='$full_name' WHERE vendorid='$vendorid' AND bus='USB';";
		DB::$instance->query($query);
	}
}

fclose($ids_fp);