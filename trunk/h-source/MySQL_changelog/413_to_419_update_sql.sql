-- Apply these queries in order to update your database from revision 413 to revision 419

alter table hardware modify kernel varchar(100) NOT NULL;
alter table revisions modify kernel varchar(100) NOT NULL;
