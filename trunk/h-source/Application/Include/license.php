<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class License
{

	//license notice that does appear at the bottom of each hardware and wiki page
	public static $bottom = array(
		'en'	=>	"The contents of this page are in the Public Domain. (see the <a href='http://creativecommons.org/publicdomain/zero/1.0/'>CC0 page</a> for detailed information). Anyone is free to copy, modify, publish, use, sell, or distribute the text for any purpose, commercial or non-commercial, and by any means.",
		'it'	=>	"I contenuti di questa pagina sono di Pubblico Dominio. (leggi la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.it'>pagina CC0</a> per informazioni dettagliate). Chiunque è libero di copiare, modificare, pubblicare, usare, vendere, o distribuire tale contenuto per qualsiasi fine, commerciale o non commerciale, e con ogni mezzo.",
		'es'	=>	"El contenido de esta página está en Dominio Público. (vea la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.es_ES'>página CC0</a> para información detallada). Cualquiera es libre de copiar, modificar, publicar, usar, vender o distribuir el texto para cualquier propósito, comercial o no comercial, y por cualquier medio.",
		'fr'	=>	"Le contenu de cette page est dans le Domaine Public (voir la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.fr'>page CC0</a> pour plus d'informations). Quiquonque est libre de copier, modifier, publier, utiliser, vendre ou distribuer le texte , quelques soit le but et le moyen.",
	);

	//license notice that does appear inside the xml download page
	public static $insideXml = array(
		'en'	=>	"The contents of this page are in the Public Domain. (see the CC0 page at http://creativecommons.org/publicdomain/zero/1.0/ for detailed information). Anyone is free to copy, modify, publish, use, sell, or distribute the text for any purpose, commercial or non-commercial, and by any means.",
		'it'	=>	"I contenuti di questa pagina sono di Pubblico Dominio. (leggi la pagina CC0 all'indirizzo http://creativecommons.org/publicdomain/zero/1.0/deed.it per informazioni dettagliate). Chiunque è libero di copiare, modificare, pubblicare, usare, vendere, o distribuire tale contenuto per qualsiasi fine, commerciale o non commerciale, e con ogni mezzo.",
		'es'	=>	"El contenido de esta página está en Dominio Público. (vea la página CC0 http://creativecommons.org/publicdomain/zero/1.0/deed.es_ES para información detallada). Cualquiera es libre de copiar, modificar, publicar, usar, vender o distribuir el texto para cualquier propósito, comercial o no comercial, y por cualquier medio.",
		'fr'	=>	"Le contenu de cette page est dans le Domaine Public (voir la page CC0 http://creativecommons.org/publicdomain/zero/1.0/deed.fr pour plus d'informations). Quiquonque est libre de copier, modifier, publier, utiliser, vendre ou distribuer le texte , quelques soit le but et le moyen.",
	);

	//license notice that does appear before the submission of each hardware and wiki page
	public static $submissionNotice = array(
		'en'	=>	"Any text submitted by you will be put in the Public Domain (see the <a href='http://creativecommons.org/publicdomain/zero/1.0/'>CC0 page</a> for detailed information).",
		'it'	=>	"Ogni testo da te inviato diventerà di Pubblico Dominio. (leggi la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.it'>pagina CC0</a> per informazioni dettagliate).",
		'es'	=>	"Cualquier texto agregado por usted será colocado en el Dominio Público (vea la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.es_ES'>página CC0</a> para información detallada).",
		'fr'	=>	"Chaque texte que vous soumettrez seront placés dans le Domaine Public (voir la <a href='http://creativecommons.org/publicdomain/zero/1.0/deed.fr'>page CC0</a> pour plus d'informations)"
	);

	//license notice that does appear before the submission of each hardware page from the h-node client (h-client)
	public static $submissionNoticeClient = array(
		'en'	=>	"Any text submitted by you will be put in the Public Domain (see the CC0 page at \nhttp://creativecommons.org/publicdomain/zero/1.0/ for detailed information).",
		'it'	=>	"Ogni testo da te inviato diventerà di Pubblico Dominio. (leggi la pagina CC0 all'indirizzo \nhttp://creativecommons.org/publicdomain/zero/1.0/deed.it per informazioni dettagliate).",
		'es'	=>	"Cualquier texto agregado por usted será colocado en el Dominio Público (vea la página CC0 \nhttp://creativecommons.org/publicdomain/zero/1.0/deed.es_ES para información detallada).",
		'fr'	=>	"Chaque texte que vous soumettrez seront placés dans le Domaine Public (voir la page CC0 \nhttp://creativecommons.org/publicdomain/zero/1.0/deed.fr pour plus d'informations)"
	);

	public static function getNotice($noticeArray)
	{
		if (array_key_exists(Lang::$current,$noticeArray))
		{
			return $noticeArray[Lang::$current];
		}
		else
		{
			return $noticeArray['en'];
		}
	}
	
	//get the bottom notice
	public static function getBottom()
	{
		return self::getNotice(self::$bottom);
	}

	//get the submission notice
	public static function getSubmissionNotice()
	{
		return self::getNotice(self::$submissionNotice);
	}

	//get the license notice insidethe xml download page
	public static function getInsideXml()
	{
		return self::getNotice(self::$insideXml);
	}

	//get the notice for the client
	public static function getClientNotice()
	{
		return self::getNotice(self::$submissionNoticeClient);
	}
}