<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class Website
{
	static public $generalMail = "";
	
	static public $fromEmail = "noreply@h-node.org";
	
	static public $generalName = "h-node.org";
	
	static public $projectName = "h-node";
	
	static public $mailServer = "";
	
	static public $mailPassword = "";

	static public $allowAnonymousSubmission = "yes";
	
	static public $statusnetGroupText = "";
	
	static public $useSMTP = true;
	
	//use a xml file the modules of the website?
	static public $useXmlConfigFile = true;
	
	//folder of the xml configuration file
	//the constant ROOT contains the path to the root folder of your installaton of h-source
	static public $xmlConfigFileFolder = ROOT;
}

class Account
{
	
	static public $confirmTime = 3600;
	
	public static function getTransport()
	{
		if (Website::$useSMTP)
		{
			return Swift_SmtpTransport::newInstance(Website::$mailServer, 25)->setUsername(Website::$generalMail)->setPassword(Website::$mailPassword);
		}
		else
		{
			return Swift_MailTransport::newInstance();
		}
	}
	
	static public function confirm($username,$e_mail,$id_user,$token)
	{
		require_once (ROOT.'/External/swiftmailer/lib/swift_required.php');
		
		$clean['username'] = sanitizeAll($username);
		$clean['id_user'] = (int)$id_user;
		$clean['token'] = sanitizeAll($token);
		
		$siteName = Website::$generalName;
		$siteMail = Website::$generalMail;
		
		$mess = gtext("Hello,\n\nyou have registered an account at")." $siteName ".gtext("with the following data:\nusername: ").$clean['username']."\n\n".gtext("in order to confirm the registration of the new account please follow the link below")."\n".Url::getRoot()."users/confirm/".Lang::$current."/".$clean['id_user']."/".$clean['token']."\n\n".gtext("If you don't want to confirm the account registration\nthen wait one hour and your username and e-mail will be deleted from the database")."\n\n".gtext("If you received this e-mail for error, please simply disregard this message");
		
		$message = Swift_Message::newInstance()->setSubject('['.Website::$projectName.'] '.gtext("account registration"))->setFrom(array(Website::$fromEmail => $siteName))->setTo(array($e_mail))->setBody($mess);

		//Create the Transport
		$transport = self::getTransport();
		
		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		//Send the message
		$result = $mailer->send($message);

		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	
	static public function sendnew($username,$e_mail,$id_user,$token)
	{
		require_once (ROOT.'/External/swiftmailer/lib/swift_required.php');
		
		$clean['username'] = sanitizeAll($username);
		$clean['id_user'] = (int)$id_user;
		$clean['token'] = sanitizeAll($token);
		
		$siteName = Website::$generalName;
		$siteMail = Website::$generalMail;
		
		$mess = "Hello,\n\nyou have requested a new password for your account at $siteName.\nYour username is:\n".$clean['username']."\n\nin order to obtain a new password for your account follow the link below\n".Url::getRoot()."users/change/".Lang::$current."/".$clean['id_user']."/".$clean['token']."\n\nIf you don't want to change the password then disregard this mail\n";
		
		$message = Swift_Message::newInstance()->setSubject('['.Website::$projectName.'] request a new password')->setFrom(array(Website::$fromEmail => $siteName))->setTo(array($e_mail))->setBody($mess);

		//Create the Transport
		$transport = self::getTransport();

		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		//Send the message
		$result = $mailer->send($message);

		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	static public function sendpassword($username,$e_mail,$password)
	{
		require_once (ROOT.'/External/swiftmailer/lib/swift_required.php');
		
		$clean['username'] = sanitizeAll($username);
		$clean['password'] = sanitizeAll($password);
		
		$siteName = Website::$generalName;
		$siteMail = Website::$generalMail;
		
		$mess = "Hello,\n\nyou have requested a new password for your account to $siteName.\nYour username is:\n".$clean['username']."\n\nYour new password is:\n".$clean['password']."\n";
		
		$message = Swift_Message::newInstance()->setSubject('['.Website::$projectName.'] get your new password ')->setFrom(array(Website::$fromEmail => $siteName))->setTo(array($e_mail))->setBody($mess);

		//Create the Transport
		$transport = self::getTransport();

		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		//Send the message
		$result = $mailer->send($message);
		
		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	static public function sendTalkNotice($who,$e_mails,$id_hard)
	{
		require_once (ROOT.'/External/swiftmailer/lib/swift_required.php');

		$who = sanitizeAll($who);
		$id_hard = (int)$id_hard;
		
		$siteName = Website::$generalName;
		$siteMail = Website::$generalMail;

		$urls = getUrlsFromIdHard($id_hard);
		
		$mess = "$who has added a message to the talk page of a device you have contributed to maintain at $siteName\n\nThe whole conversation is here:\n\n".$urls['urlTalk']."\n\nThe device page is here:\n\n".$urls['urlView']."\n\nBest regards\nthe ".Website::$projectName." team\n\nP.S: you can disable the mail notifications in the profile page of your control panel";

		$message = Swift_Message::newInstance()->setSubject("[".Website::$projectName."] $who sent a notice to your attention")->setFrom(array(Website::$fromEmail => $siteName))->setTo($e_mails)->setBody($mess);

		//Create the Transport
		$transport = self::getTransport();

		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		//Send the message
		$result = $mailer->batchSend($message);

	}

	static public function sendWikiTalkNotice($who,$e_mails,$id_wiki)
	{
		require_once (ROOT.'/External/swiftmailer/lib/swift_required.php');

		$who = sanitizeAll($who);
		$id_wiki = (int)$id_wiki;

		$siteName = Website::$generalName;
		$siteMail = Website::$generalMail;

		$wiki = new WikiModel();
		$pageUrl = $wiki->toWikiPage($id_wiki);
		$domainName = rtrim(Url::getRoot(),"/");
		$talkUrl = $domainName."/wiki/talk/".Lang::$current."/$id_wiki";
		
		$mess = "$who has added a message to the talk page of a wiki page you have contributed to maintain at $siteName\n\nThe whole conversation is here:\n\n".$talkUrl."\n\nThe wiki page is here:\n\n".$pageUrl."\n\nBest regards\nthe ".Website::$projectName." team\n\nP.S: you can disable the mail notifications in the profile page of your control panel";

		$message = Swift_Message::newInstance()->setSubject("[".Website::$projectName."] $who sent a notice to your attention")->setFrom(array(Website::$fromEmail => $siteName))->setTo($e_mails)->setBody($mess);

		//Create the Transport
		$transport = self::getTransport();

		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		//Send the message
		$result = $mailer->batchSend($message);

	}
}