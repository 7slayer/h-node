<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class NotebooksModel extends GenericModel {

	public $type = 'notebook'; //device type
	
	public function __construct()
	{
   
		$this->_popupItemNames = array(
			'vendor'		=>	'vendor',
			'compatibility'	=>	'compatibility',
			'comm_year'		=>	'comm_year',
			'subtype'		=>	'subtype',
		);
   
		$this->_popupLabels = array(
			'vendor'		=>	gtext("vendor"),
			'compatibility'	=>	gtext("compatibility"),
			'comm_year'		=>	gtext("year"),
			'subtype'		=>	gtext("subtype"),
		);

		$this->setPopupFunctions();
		
		$this->createPopupWhere('vendor,compatibility,comm_year,subtype');
		
		$this->diffFields = array(
			'vendor' 		=>	gtext("vendor"),
			'model' 		=>	gtext('model name'),
			'subtype' 		=>	gtext('subtype (notebook or netbook)'),
			'architecture' 	=>	gtext('architecture'),
			'bios'			=>	gtext('does it have a free bios?'),
			'can_free_systems_be_installed'			=>	gtext('can free operating systems be installed?'),
			'prevent_wifi'			=>	gtext('does the device prevent the installation of wifi cards not-approved by the vendor?'),
			'comm_year'		=>	gtext('year of commercialization'),
			'distribution' 	=>	gtext('GNU/Linux distribution used for the test'),
			'compatibility'	=>	gtext('compatibility with free software'),
			'kernel'		=>	gtext('tested with the following kernel libre'),
			'video_card_type' 	=>	gtext('video card model'),
			'video_card_works' 	=>	gtext('does the video card work?'),
			'wifi_type'		=>	gtext('wifi model'),
			'wifi_works'	=>	gtext('does the wifi card work?'),
			'webcam_type'	=>	gtext('webcam model'),
			'webcam_works'	=>	gtext('does the webcam work?'),
			'description'	=>	gtext('Description'),
		);
	
		$this->fieldsWithBreaks = array(gtext('Description'));
			
		parent::__construct();
	}

	public function morePopups()
	{
		$this->_popupItemNames = array(
			'bios'			=>	'bios',
			'architecture'	=>	'architecture',
		);

		$this->_popupLabels = array(
			'bios'			=>	gtext("free boot firmware?"),
			'architecture'	=>	gtext("architecture"),
		);

		$this->_popupFunctions = array(
			'architecture'		=>	'translate_and_gtext',
			'bios'				=>	'translate_and_gtext',
		);
		
		$this->createPopupWhere('bios,architecture');
	}

}