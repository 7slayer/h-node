<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class IssuesModel extends BaseModel {

	public function __construct() {
		$this->_tables = 'issues';
		$this->_idFields = 'id_issue';
		
		$this->_onDelete = 'nocheck';
		
		$this->_where = array(
			'priority'	=>	'issues',
			'status'	=>	'issues',
			'topic'		=>	'issues',
		);
		
		$this->_popupItemNames = array(
			'priority'	=>	'priority',
			'status'	=>	'status',
			'topic'		=>	'topic',
			'deleted'	=>	'deleted',
		);
		
		$this->_popupLabels = array(
			'priority'	=>	'PRIORITY',
			'status'	=>	'STATUS',
			'topic'		=>	'TOPIC',
			'deleted'	=>	'DELETED?',
		);
		
		$this->orderBy = 'issues.id_issue desc';
		
		$this->strongConditions['insert'] = array(
			"checkLength|99"	=>	'title',
			"+checkLength|34"	=>	'topic',
			"++checkLength|15"	=>	'priority',
			"+++checkLength|5000"	=>	'message',
			"checkisStrings|low,medium,high"	=>	'priority',
			"+checkisStrings|maybe-a-bug,new-categories-of-hardware,add-a-vendor-name,other"	=>	'topic',
		);
		
		$this->formStruct = array(
			'entries' 	=> 	array(
				'title'	=> 	array('labelString'	=>	gtext("TITLE").':'),
				'topic'	=> 	array(
					'type'=>'Select',
					'options'=>array(
						'Add a vendor name'					=>	'add-a-vendor-name',
						'Maybe a bug' 						=>	'maybe-a-bug',
						'Add new categories of hardware'	=>	'new-categories-of-hardware',
						'Other'								=>	'other'
					),
					'labelString'	=>	gtext("TOPIC").':',
				),
				'deleted'=> array(
					'type'		=>	'Select',
					'options'	=>	'no,yes',
				),
				'priority'	=> 	array(
					'type'=>'Select',
					'options'=>'low,medium,high',
					'labelString'	=>	gtext("PRIORITY").':',
				),
				'message'	=> 	array('type'=>'Textarea','idName'=>'bb_code','labelString'	=>	gtext("MESSAGE").':',),
				'status'	=>	array(
					'type'		=>	'Select',
					'options'	=>	'opened,closed'
				),
				'notice'		=>	array(
					'type'		=>	'Textarea',
					'idName'	=>	'bb_code_notice',
				),
				'id_issue'		=>	array(
					'type'		=>	'Hidden'	
				)
			),
		);
	
		parent::__construct();
	}

	public function pUpdate($id)
	{
		return parent::update($id);
	}

	public function insert()
	{
		if (parent::insert())
		{
			//update the history
			$this->updateHistory('issue_ins');
		}
	}

}