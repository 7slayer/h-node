<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class BaseModel extends Model_Tree
{

	public function updateHistory($type)
	{
		$clean['created_by'] = (int)$this->values['created_by'];
		$clean['last_id'] = (int)$this->lastId();
		
		$history = new HistoryModel();
		$history->values = array(
			'created_by' 	=>	$clean['created_by'],
			'type'			=>	sanitizeAll($type),
			'action'		=>	"insert",
			'id'			=>	$clean['last_id'],
			'message'		=>	'',
			'gr'			=>	'registered',
		);
		$history->insert();
	}

}