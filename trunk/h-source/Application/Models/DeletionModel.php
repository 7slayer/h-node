<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class DeletionModel extends Model_Tree
{

	public function __construct()
	{
		$this->_tables = 'deletion';
		$this->_idFields = 'id_del';
		
// 		$this->_where=array(
// 			'id_hard'	=>	'talk'
// 		);
// 		
		$this->_popupItemNames = array(
			'object'	=>	'object',
		);
		
		$this->_popupLabels = array(
			'object'	=>	'OBJECT',
		);
// 		
		$this->orderBy = 'deletion.id_del desc';
		
		$this->strongConditions['insert'] = array(
			"checkIsStrings|duplication,other"	=>	'object',
			"+checkLength|500"					=>	'message'
		);
		
		parent::__construct();
	}

}