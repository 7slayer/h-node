<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class GenericModel extends Model_Tree {

	public $type = ''; //device type
	public $diffFields = array();
	public $fieldsWithBreaks = array();
	
	public function __construct() {
		$this->_tables = 'hardware';
		$this->_idFields = 'id_hard';
		
		$this->_where=array(
			'type'			=>	'hardware',
			'vendor'		=>	'hardware',
			'compatibility'	=>	'hardware',
			'comm_year'		=>	'hardware',
		);
		
		$this->orderBy = 'hardware.id_hard desc';
		parent::__construct();
	}

	public function checkType($id_hard = 0)
	{
		$clean['id_hard'] = (int)$id_hard;
		$res = $this->db->select('hardware','type','id_hard='.$clean['id_hard']);
		if (count($res) > 0)
		{
			return (strcmp($this->type,$res[0]['hardware']['type']) === 0 ) ? true : false;
		}
		return false;
	}
	
	public function getDiffArray($oldArray, $newArray)
	{
		return getDiffArray($this->diffFields, $oldArray, $newArray);
	}

	//create the $_popupWhere property
	public function createPopupWhere($list)
	{
		$listArray = explode(',',$list);
		foreach ($listArray as $field)
		{
			$this->_popupWhere[$field] = 'type="'.$this->type.'" and deleted="no" and cleared="no"';
		}
	}

	//set the fuction to be applied upon each popup name
	public function setPopupFunctions()
	{
		foreach ($this->_popupItemNames as $name => $field)
		{
			$this->_popupFunctions[$name] = getTranslationFunction($name);
		}
	}

	//get the HTML of the popup labels
	public function getPopupLabel($viewArgs)
	{
		$html = null;
		$listArray = array('page','history_page','search_string');

		$count = 0;
		
		foreach ($viewArgs as $field => $value)
		{
			if ($count < 5)
			{
				if (!in_array($field,$listArray))
				{
					$value = call_user_func(getTranslationFunction($field),$value);
					$html .= "<div class='viewall_popup_menu_status_item'>".$value."</div>\n";

					$count++;
				}
			}
		}
		return $html;
	}

}