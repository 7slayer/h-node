<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class DownloadController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
		
		$this->_topMenuClasses['download'] = " class='currentitem'";
		
		parent::__construct($model, $controller, $queryString);
		
		$this->model('HardwareModel');
		
		$data['title'] = 'download - '.Website::$generalName;
		$this->append($data);
	}
	
	public function index($lang = 'en')
	{		
		$this->cleverLoad('index');
		$this->right($lang);
	}

	//get the xml of the archive
	private function getXml($res)
	{
		$xml = null;
		$xml = "<?xml version='1.0' encoding='UTF-8'?>\n";
		$xml .= "<hardware>\n";
		$xml .= "\t<general_information>\n";
		$xml .= "\t\t<credits>h-node project</credits>\n";
		$xml .= "\t\t<link>".Website::$generalName."</link>\n";
		$xml .= "\t\t<date>".date("Y-m-d H:i:s")."</date>\n";
		$xml .= "\t\t<license>".License::getInsideXml()."</license>\n";
		$xml .= "\t</general_information>\n";
		
		foreach ($res as $row)
		{			
			$type = $row['hardware']['type'];
			$xml .= "\t<device>\n";
			
			$xml .= "\t\t<id>".$row['hardware']['id_hard']."</id>\n";
			$xml .= "\t\t<type>".$row['hardware']['type']."</type>\n";
			
			if ($type === 'notebook' or $type === 'printer' or $type === 'host-controller' )
			{
				$xml .= "\t\t<subtype>".$row['hardware']['subtype']."</subtype>\n";
			}
			if ($type === 'notebook')
			{
				$xml .= "\t\t<architecture>".translate($row['hardware']['architecture'])."</architecture>\n";
				$xml .= "\t\t<free_bios>".$row['hardware']['bios']."</free_bios>\n";
				$xml .= "\t\t<can_free_systems_be_installed>".$row['hardware']['can_free_systems_be_installed']."</can_free_systems_be_installed>\n";
				$xml .= "\t\t<prevent_wifi>".$row['hardware']['prevent_wifi']."</prevent_wifi>\n";
			}
			
			$xml .= "\t\t<model_name>".$row['hardware']['model']."</model_name>\n";
			
			if ($type !== 'notebook')
			{
				$xml .= "\t\t<other_names><![CDATA[".$row['hardware']['other_names']."]]></other_names>\n";
				$xml .= "\t\t<vendorid_productid>".$row['hardware']['pci_id']."</vendorid_productid>\n";
			}
			
			$xml .= "\t\t<vendor>".$row['hardware']['vendor']."</vendor>\n";
			$xml .= "\t\t<kernel_libre>".$row['hardware']['kernel']."</kernel_libre>\n";
			$xml .= "\t\t<distribution>".$row['hardware']['distribution']."</distribution>\n";
			$xml .= "\t\t<year>".$row['hardware']['comm_year']."</year>\n";
			
			if ($type !== 'notebook')
			{
				$xml .= "\t\t<interface>".$row['hardware']['interface']."</interface>\n";
			}
			
			if ($type === 'notebook' or $type === 'printer' or $type === 'scanner')
			{
				$xml .= "\t\t<compatibility>".$row['hardware']['compatibility']."</compatibility>\n";
			}
			else if ($type === '3G-card' or $type === 'wifi')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['wifi_works']."</it_works>\n";
			}
			else if ($type === 'soundcard')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['sound_card_works']."</it_works>\n";
			}
			else if ($type === 'bluetooth')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['bluetooth_works']."</it_works>\n";
			}
			else if ($type === 'webcam')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['webcam_works']."</it_works>\n";
			}
			else if ($type === 'videocard')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['video_card_works']."</it_works>\n";
			}
			else if ($type === 'acquisition-card' or $type === 'modem' or $type === 'RAID-adapter' or $type === 'host-controller')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['compatibility']."</it_works>\n";
			}
			else if ($type === 'fingerprint-reader')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['fingerprint_works']."</it_works>\n";
			}
			else if ($type === 'ethernet-card')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['ethernet_card_works']."</it_works>\n";
			}
			else if ($type === 'sd-card-reader')
			{
				$xml .= "\t\t<it_works>".$row['hardware']['sd_card_works']."</it_works>\n";
			}
			
			if ($type === 'printer')
			{
				$xml .= "\t\t<it_tracks_users>".$row['hardware']['it_tracks_users']."</it_tracks_users>\n";
			}
			
			if ($type === 'notebook')
			{
				$xml .= "\t\t<wifi_card>".$row['hardware']['wifi_type']."</wifi_card>\n";
				$xml .= "\t\t<videocard>".$row['hardware']['video_card_type']."</videocard>\n";
				$xml .= "\t\t<webcam>".$row['hardware']['webcam_type']."</webcam>\n";
				$xml .= "\t\t<wifi_works>".$row['hardware']['wifi_works']."</wifi_works>\n";
				$xml .= "\t\t<videocard_works>".$row['hardware']['video_card_works']."</videocard_works>\n";
				$xml .= "\t\t<webcam_works>".$row['hardware']['webcam_works']."</webcam_works>\n";
			}
			
			if ($type !== "notebook")
			{
				$xml .= "\t\t<driver>".$row['hardware']['driver']."</driver>\n";
			}
			
			$xml .= "\t\t<description><![CDATA[".$row['hardware']['description']."]]></description>\n";
			
			$xml .= "\t\t<link>".$this->baseUrl."/".Hardware::getControllerFromType($type)."/view/".$this->lang."/".$row['hardware']['id_hard']."/".encodeUrl($row['hardware']['model'])."</link>\n";
			$xml .= "\t\t<created_by>".$this->baseUrl."/".Hardware::getControllerFromType($type)."/history/".$this->lang."/".$row['hardware']['id_hard']."</created_by>\n";
			
			$xml .= "\t</device>\n";
		}
		
		$xml .= "</hardware>\n";
		
		return $xml;
	}

	protected function printXml($lang = 'en', $whereArray)
	{
		header ("Content-Type:text/xml");

// 		header("Content-Type: application/force-download");
// 		header("Content-Type: application/octet-stream");
// 		header("Content-Type: application/download");

		$filename = array_key_exists('type',$whereArray) ? $whereArray['type'] : "all";
		header("Content-Disposition: attachment; filename=h-node-".$filename.".xml;");
		
		$res = $this->m['HardwareModel']->clear()->select()->where($whereArray)->aWhere(array("cleared"=>"no"))->orderBy("type,hardware.id_hard")->send();

		$data['xml'] = $this->getXml($res);

		$this->append($data);
		$this->clean();
		$this->load('xml');
	}

	public function all($lang = 'en')
	{
		$this->printXml($lang, array('-deleted'=>'no'));
	}

	public function notebooks($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'notebook','-deleted'=>'no'));
	}
	
	public function wifi($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'wifi','-deleted'=>'no'));
	}
	
	public function videocards($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'videocard','-deleted'=>'no'));
	}

	public function printers($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'printer','-deleted'=>'no'));
	}
	
	public function scanners($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'scanner','-deleted'=>'no'));
	}
	
	public function threegcards($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'3G-card','-deleted'=>'no'));
	}
	
	public function soundcards($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'soundcard','-deleted'=>'no'));
	}

	public function webcams($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'webcam','-deleted'=>'no'));
	}

	public function bluetooth($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'bluetooth','-deleted'=>'no'));
	}

	public function acquisitioncards($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'acquisition-card','-deleted'=>'no'));
	}

	public function fingerprintreaders($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'fingerprint-reader','-deleted'=>'no'));
	}

	public function ethernetcards($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'ethernet-card','-deleted'=>'no'));
	}

	public function sdcardreaders($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'sd-card-reader','-deleted'=>'no'));
	}

	public function modems($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'modem','-deleted'=>'no'));
	}
	
	public function raidadapters($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'RAID-adapter','-deleted'=>'no'));
	}
	
	public function hostcontrollers($lang = 'en')
	{
		$this->printXml($lang, array('type'=>'host-controller','-deleted'=>'no'));
	}
}