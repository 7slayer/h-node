<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class ClientController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);
	}
	
	public function licenseinfo($lang = 'en')
	{
		header ("Content-Type:text/xml");
		
		$data["xml"] = "<?xml version='1.0' encoding='UTF-8'?>\n";
		
		$this->append($data);
		$this->clean();
		$this->load("license");
	}

	public function userinfo($lang = 'en')
	{
		header ("Content-Type:text/xml");
		
		$this->s['registered']->checkStatus();

		$data['user_status'] = strcmp($this->islogged,'yes') === 0 ? 'logged' : 'not-logged';

		$this->append($data);
		$this->clean();
		$this->load('info');
	}

}