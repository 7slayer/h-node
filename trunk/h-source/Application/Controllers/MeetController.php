<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class MeetController extends BaseController
{
	private $whereArray = array(
		'has_confirmed'	=>	0,
		'deleted'		=>	'no',
	);
	
	public function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);
		
		$this->model('ProfileModel');
		$this->model('HardwareModel');
		$this->model('IssuesModel');
		$this->model('MessagesModel');
		$this->model('TalkModel');
		$this->model('WikiModel');
		$this->model('WikitalkModel');
		
		$argKeys = array(
			'page:forceNat'	=>	1,
		);

		$this->setArgKeys($argKeys);
		
		$data['title'] = 'meet - '.Website::$generalName;
		$this->append($data);
	}
	
	//public page of the user
	public function user($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "meet ".$clean['user']." - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);
			$data['meet_id_user'] = $clean['id_user'];
			
			$data['isBlocked'] = $this->m['UsersModel']->isBlocked($clean['id_user']);
			
			$this->whereArray['username'] = $clean['user'];
			
			$data['table'] = $this->m['ProfileModel']->select('regusers.e_mail,regusers.username,profile.*')->from('regusers inner join profile')->on('regusers.id_user = profile.created_by')->where($this->whereArray)->send();
			
			$data['meet_username'] = $clean['user'];
			
// 			javascript for moderator
			$data['md_javascript'] = "moderator_dialog(\"block\",\"user\");moderator_dialog(\"unblock\",\"user\");";
		
			$this->append($data);
			$this->load('meet');
			$this->load('moderator_dialog');
			$this->right();
		}

	}

	//contributions menu
	public function contributions($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = $clean['user']." contributions - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);
			
			$this->whereArray['username'] = $clean['user'];
			
			$data['meet_username'] = $clean['user'];
			
			$hardNumb = $this->m['HardwareModel']->select()->where($this->whereArray)->rowNumber();
			
			$issuesNumb = $this->m['IssuesModel']->select('id_issue')->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->rowNumber();
			
			$messNumb = $this->m['MessagesModel']->select('id_mes,messages.id_issue,messages.creation_date')->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->rowNumber();
			
			$talkNumb = $this->m['TalkModel']->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->rowNumber();

			$wikiNumb = $this->m['WikiModel']->where($this->whereArray)->rowNumber();

			$wikiTalkNumb = $this->m['WikitalkModel']->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->rowNumber();
			
			$data['hasHardware'] = $hardNumb > 0 ? true : false;
			$data['hasIssues'] = $issuesNumb > 0 ? true : false;
			$data['hasMessages'] = $messNumb > 0 ? true : false;
			$data['hasTalk'] = $talkNumb > 0 ? true : false;
			$data['hasWiki'] = $wikiNumb > 0 ? true : false;
			$data['hasWikiTalk'] = $wikiTalkNumb > 0 ? true : false;
			
			$this->append($data);
			$this->load('contributions');
			$this->right();
		}

	}

	//hardware contributions
	public function hardware($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "hardware contributions - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			$this->whereArray['username'] = $clean['user'];
			
			$data['table'] = $this->m['HardwareModel']->select('hardware.*,regusers.username')->where($this->whereArray)->send();
			
			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "hardware contributions";
			$data['page_explanation_title'] = "hardware contributions of";
			
			$this->append($data);
			$this->load('list_template');
			$this->right();
		}

	}

	//issues opened
	public function issues($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "issues submitted - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			//load the Pages helper
			$this->helper('Pages','meet/issues/'.$this->lang.'/'.$clean['user'],'page');
		
			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);

			$this->m['IssuesModel']->select('id_issue')->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->orderBy('id_issue desc');
			
			$recordNumber = $this->m['IssuesModel']->rowNumber();
			$page = $this->viewArgs['page'];
			
			$this->m['IssuesModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,20);
			
			$data['table'] = $this->m['IssuesModel']->select('id_issue,creation_date,title')->send();
			
			$data['pageList'] = $this->h['Pages']->render($page-3,7);
			
			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "issues opened";
			$data['page_explanation_title'] = "issues opened by";
			
			$this->append($data);
			$this->load('list_template');
			$this->right();
		}

	}

	//messages submitted
	public function messages($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "messages submitted - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			//load the Pages helper
			$this->helper('Pages','meet/messages/'.$this->lang.'/'.$clean['user'],'page');
			
			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);

			$this->m['MessagesModel']->from('messages inner join issues')->on('messages.id_issue=issues.id_issue')->select('id_mes,messages.id_issue,messages.creation_date,title')->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->orderBy('id_mes desc');
			
			$recordNumber = $this->m['MessagesModel']->rowNumber();
			$page = $this->viewArgs['page'];
			
			$this->m['MessagesModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,20);
			
			$data['table'] = $this->m['MessagesModel']->send();
			
			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "messages submitted";
			$data['page_explanation_title'] = "messages submitted by";
			
			$data['pageList'] = $this->h['Pages']->render($page-3,7);
			
			$this->append($data);
			$this->load('list_template');
			$this->right();
		}
	}

	//messages in the talk page of the devices
	public function talk($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "talk messages submitted - ".Website::$generalName;
		
		$this->shift(2);
		
		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			//load the Pages helper
			$this->helper('Pages','meet/talk/'.$this->lang.'/'.$clean['user'],'page');
			
			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);

			$this->m['TalkModel']->inner('hardware')->using('id_hard')->select('talk.*,hardware.*')->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->orderBy('id_talk desc');
			
			$recordNumber = $this->m['TalkModel']->rowNumber();
			$page = $this->viewArgs['page'];
			
			$this->m['TalkModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,20);
			
			$data['table'] = $this->m['TalkModel']->send();
			
			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "talk messages (hardware pages)";
			$data['page_explanation_title'] = "talk messages (hardware pages) submitted by";
			
			$data['pageList'] = $this->h['Pages']->render($page-3,7);
			
			$this->append($data);
			$this->load('list_template');
			$this->right();
		}
	}

	//hardware contributions
	public function wiki($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "wiki contributions - ".Website::$generalName;

		$this->shift(2);

		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			//load the Pages helper
			$this->helper('Pages','meet/wiki/'.$this->lang.'/'.$clean['user'],'page');
			
			$this->whereArray['username'] = $clean['user'];

			$data['table'] = $this->m['WikiModel']->select('wiki.*,regusers.username')->where($this->whereArray)->orderBy('wiki.id_wiki desc');

			$recordNumber = $this->m['WikiModel']->rowNumber();
			$page = $this->viewArgs['page'];

			$this->m['WikiModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,30);

			$data['table'] = $this->m['WikiModel']->send();
			
			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "wiki contributions";
			$data['page_explanation_title'] = "wiki contributions of";
			
			$data['pageList'] = $this->h['Pages']->render($page-5,11);
			
			$this->append($data);
			$this->load('list_template');
			$this->right();
		}

	}

	//messages in the talk page of the wiki pages
	public function wikitalk($lang = 'en', $user = '')
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';
		$data['title'] = "talk messages submitted - ".Website::$generalName;

		$this->shift(2);

		if ($this->m['UsersModel']->userExists($clean['user']))
		{
			//load the Pages helper
			$this->helper('Pages','meet/wikitalk/'.$this->lang.'/'.$clean['user'],'page');

			$clean['id_user'] = (int)$this->m['UsersModel']->getUserId($clean['user']);

			$this->m['WikitalkModel']->where(array('created_by'=>$clean['id_user'],'deleted'=>'no'))->orderBy('id_talk desc');

			$recordNumber = $this->m['WikitalkModel']->rowNumber();
			$page = $this->viewArgs['page'];

			$this->m['WikitalkModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,20);

			$data['table'] = $this->m['WikitalkModel']->send();

			$data['meet_username'] = $clean['user'];

			$data['tree_last_string'] = "talk messages (wiki pages)";
			$data['page_explanation_title'] = "talk messages (wiki pages) submitted by";

			$data['pageList'] = $this->h['Pages']->render($page-3,7);

			$this->append($data);
			$this->load('list_template');
			$this->right();
		}
	}
}