<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; credits
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Icone:
			</div>
			
			<div class="credits_item_description">
				Le icone utilizzate nel sito h-node.com appartengono ai temi di icone <a href="http://kde-look.org/content/show.php/ACUN+Simgeleri?content=83018">ACUN Simgeleri 0.7</a> e <a href="http://kde-look.org/content/show.php/H2O+Icon+Theme?content=127149">H2O Icon Theme 0.0.5</a>, entrambi sotto licenza GNU GPL, e al tema <a href="http://www.everaldo.com/crystal/?action=downloads">Crystal Projects</a>, sotto licenza LGPL, al <a href="http://www.notmart.org/index.php/Graphics">set di icone glaze</a> (LGPL) e al tema <a href="http://kde-look.org/content/show.php/Dark-Glass+reviewed?content=67902">DarkGlass_Reworked</a> (GPL). Le icone bandiere derivano dal set di icone <a href="http://www.famfamfam.com/lab/icons/flags/">FAMFAMFAM</a> (Public Domain).
			</div>
			
						
			<div class="credits_item_title">
				jQuery:
			</div>
			
			<div class="credits_item_description">
				Le librerie javascript <a href="http://jquery.com/">jQuery</a> , <a href="http://jqueryui.com/home">jQuery UI</a> e <a href="http://jquerymobile.com/">jQuery Mobile</a> (sotto licenza MIT/GPL) sono state usate nel sito
			</div>

			<div class="credits_item_title">
				markitup:
			</div>
			
			<div class="credits_item_description">
				Il plugin jQuery <a href="http://markitup.jaysalvat.com/home/">markitup</a> (sotto licenza MIT/GPL) è stato usato per aiutare gli utenti a inserire i tag della wiki			</div>
			
			<div class="credits_item_title">
				Algoritmo php diff:
			</div>
			
			<div class="credits_item_description">
				<a href="http://compsci.ca/v3/viewtopic.php?p=142539">Questo</a> algoritmo (sotto licenza libera zlib) è stato usato per sottolineare la differenza tra due diverse revisioni dello stesso modello di hardware.
			</div>


		</div>
	
	</div>
