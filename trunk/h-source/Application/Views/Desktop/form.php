<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>
	
		$(document).ready(function() {
			
			dist_list_helper();
			
			$("#bb_code").markItUp(mySettings);
			
		});
		
	</script>

	<?php echo $notice;?>

	<div class="notebooks_insert_form">
		<form action="<?php echo $this->baseUrl."/".$this->controller."/".$this->action."/$lang/$token".$this->viewStatus;?>" method="POST">
		
			<div class="edit_form">
			
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("model name");?>: <b>*</b><?php echo $modelNameLabel;?><div class="entry_label_small"><?php echo gtext("Write here the model name obtained by the lspci or lsusb command.");?></div></div>
					<?php echo Html_Form::input('model',$values['model'],'input_entry');?>
				</div>

				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("possible other names of the device");?>:<br /><span class="entry_label_small"><?php echo gtext("Add here the device name as written on the device itself or on the device box. Add it only if it is different from the <i>model name</i> already inserted inside the above entry. Add the new name in such a way that there is one name per row.");?></span></div>
					<?php echo Html_Form::textarea('other_names',$values['other_names'],'device_textarea_entry');?>
				</div>

				<?php if (strcmp($this->controller,'printers') === 0 ) { ?>
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("subtype");?> (laser, inkjet, ..):</div>
					<?php echo Html_Form::select('subtype',$values['subtype'],Printer::$subtype,"select_entry");?>
				</div>
				<?php } else if (strcmp($this->controller,'hostcontrollers') === 0 ) { ?>
					<div class="entry_label"><?php echo gtext("subtype");?> (<?php echo $subtypeHelpLabel;?>):</div>
					<?php echo Html_Form::select('subtype',$values['subtype'],Hostcontrollers::$subtype,"select_entry");?>
				<?php } ?>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("VendorID:ProductID code of the device");?>: <b>*</b><?php echo $vendoridProductidLabel;?></div>
					<?php echo Html_Form::input('pci_id',$values['pci_id'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("year of commercialization");?></div>
					<?php echo Html_Form::select('comm_year',$values['comm_year'],Hardware::getCommYears(),"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("interface");?> (<?php echo gtext("set not-specified if not sure");?>)</div>
					<?php echo Html_Form::select('interface',$values['interface'],$intefaceOptions,"select_entry");?>
				</div>
				
				<div class="form_entry td_with_distribution_checkboxes">
					<div class="entry_label"><?php echo gtext("GNU/Linux distribution used for the test");?>: <b>*</b></div>
					<?php include(ROOT . DS . APPLICATION_PATH . DS . 'Views' . DS . Params::$viewSubfolder. DS . 'noscript_distributions.php');?>
					<?php echo Html_Form::input('distribution',$values['distribution'],'input_entry input_distribution');?>
					<?php echo Distributions::getFormHtml();?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("tested with the following kernel libre");?>:<br /><span class="entry_label_small"><?php echo gtext("Write a comma-separated list of kernel versions");?><br /><?php echo gtext("Example");?>: 2.6.35-28-generic, 2.6.38-11 </span></div>
					<?php echo Html_Form::input('kernel',$values['kernel'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label hidden_x_explorer">
						<?php
						if (strcmp($this->controller,'printers') === 0  or strcmp($this->controller,'scanners') === 0 )
						{
							echo gtext("compatibility with free software").":";
						}
						else
						{
							echo gtext("does it work with free software?");
						}
						?>
					</div>
					<?php echo Html_Form::select($worksField,$values[$worksField],$worksOptions,"select_entry hidden_x_explorer");?>
					<?php if (strcmp($this->controller,'printers') === 0 or strcmp($this->controller,'scanners') === 0) {
						switch ($this->controller)
						{
							case 'printers':
								$fragment = 'Printers';
								break;
							case 'scanners':
								$fragment = 'Scanners';
								break;
						}
					?>
					<!--if it is a printer-->
					<a class="open_help_window" title="compatibility help page" target="blank" href="<?php echo $this->baseUrl."/wiki/page/$lang/Compatibility-classes#$fragment";?>"><img class="top_left_images_help" src="<?php echo $this->baseUrl;?>/Public/Img/Acun/help_hint.png"></a>
					<?php } ?>
				</div>

				<?php if (strcmp($this->controller,'printers') === 0 ) { ?>
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does it adopt any techniques to track users?");?><?php echo $tracksHelpLabel;?></div>
					<?php echo Html_Form::select('it_tracks_users',$values['it_tracks_users'],Hardware::$trackSelect,"select_entry");?>
				</div>
				<?php } ?>
				
				<div class="form_entry">
					<div class="entry_label hidden_x_explorer"><?php echo gtext("free driver used");?> (<?php echo gtext("see the help page or leave blank if you are not sure");?>):</div>
					<?php echo Html_Form::input('driver',$values['driver'],'input_entry');?>
				</div>

				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("Description: (write here all the useful information)");?><?php echo $descriptionLabel;?></div>
					<?php if (strcmp($this->controller,'threegcards') === 0 ) { ?>
					<!--if it is a 3G-card-->
					<div class="isp_notice"><?php echo gtext("Please specify in the below description entry the Internet Service Provider (ISP) and the country where the service is provided");?></div>
					<?php } ?>
					
					<?php if (isset($descriptionPreview)) { ?>
					<div class="description_preview_title"><?php echo gtext("Description entry preview");?>:</div>
					<div class="description_preview">
						<?php echo decodeWikiText($descriptionPreview); ?>
					</div>
					<?php } ?>
					
					<?php echo Html_Form::textarea('description',$values['description'],'textarea_entry','bb_code');?>
				</div>
				
				<?php echo $hiddenInput;?>

				<input type="submit" name="previewAction" value="Preview">
				<input type="submit" name="<?php echo $submitName;?>" value="Save">
				
				<div class="mandatory_fields_notice">
					<?php echo gtext("Fields marked with <b>*</b> are mandatory");?>
				</div>
				
			</div>

		</form>
	</div>