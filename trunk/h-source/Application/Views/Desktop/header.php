<?php if (!defined('EG')) die('Direct access not allowed!'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>
<?php
$u = new UsersModel();
$hw = new HardwareModel();
$wiki = new WikiModel();
$translations = array('insert'=>'inserted','update'=>'updated');
$currPos = $querySanitized ? $this->controller."/".$this->action : 'home/index';
?>
<head>

	<title><?php echo $title;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="free software project with the aim of collecting information about the hardware that works with a fully free operating system" />
	<meta name="keywords" content="hardware database, free software, GNU/Linux distribution, wiki, users freedom" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl?>/Public/Css/main.css">
	<link rel="Shortcut Icon" href="<?php echo $this->baseUrl?>/Public/Img/tab_icon_2.ico" type="image/x-icon">

	<!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Css/explorer7.css">
	<![endif]-->

	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/functions.js"></script>

	<!--markitup-->
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/markitup/jquery.markitup.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/markitup/sets/bbcode/set.js"></script>

	<!-- markItUp! skin -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Js/markitup/skins/simple/style.css" />
	<!--  markItUp! toolbar skin -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Js/markitup/sets/bbcode/style.css" />

	<!-- 	jQuery ui -->
	<link rel="stylesheet" href="<?php echo $this->baseUrl;?>/Public/Js/jquery/ui/css/excite-bike/jquery-ui-1.8.14.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/jquery/ui/js/jquery-ui-1.8.21.custom.js"></script>
	
	<script type="text/javascript">
	/*    
	@licstart  The following is the entire license notice for the 
	JavaScript code in this page.

	h-source, a web software to build a community of people that want to share their hardware information.
	Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)

	This file is part of h-source

	h-source is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	h-source is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with h-source.  If not, see <http://www.gnu.org/licenses/>.   

	@licend  The above is the entire license notice
	for the JavaScript code in this page.
	*/
	</script>
	
	<script type="text/javascript">
	
		var base_url = "<?php echo $this->baseUrl;?>";
		var curr_lang = "<?php echo $lang;?>";
		var csrf_token = "<?php echo $token;?>";

		$(document).ready(function() {

			animateTabs(curr_lang);

		});
		
	</script>
	
</head>
<body>


<div id="external_header">
	<div id="header">
		<img src="<?php echo $this->baseUrl;?>/Public/Img/title.png">
	</div>
</div>

<div id="top_menu_external">
	<div id="top_menu">
		<ul>
			<li<?php echo $tm['home']; ?>><a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a></li><li<?php echo $tm['hardware']; ?>><a href="<?php echo $this->baseUrl."/hardware/catalogue/$lang";?>"><?php echo gtext("Hardware");?></a></li><li<?php echo $tm['issues']; ?>><a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>"><?php echo gtext("Issues");?></a></li><li<?php echo $tm['search']; ?>><a href="<?php echo $this->baseUrl."/search/form/$lang";?>"><?php echo gtext("Search");?></a></li><li<?php echo $tm['download']; ?>><a href="<?php echo $this->baseUrl."/download/index/$lang";?>"><?php echo gtext("Download");?></a></li><?php echo $topMenuHelpLink;?><li<?php echo $tm['wiki']; ?>><a href="<?php echo $this->baseUrl."/wiki/page/$lang/Main-Page";?>">Wiki</a></li><?php echo $topMenuFaqLink;?>
		</ul>
	</div>
</div>
	
<div id="container">
