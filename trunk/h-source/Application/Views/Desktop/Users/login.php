<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<?php if ( strcmp($updating_flag,'no') === 0 ) { ?>

		<?php if (isset($flag)) { ?>
		<div class="login_note">
			Sorry.. you have to be logged if you want to insert a new device in the archive or modify an existing one..
		</div>
		<?php } ?>

		<?php echo $notice; ?>

		<div class="login_box">
			<form action = '<?php echo $action;?>' method = 'POST'>
			
				<table>
					<tr>
						<td>Username</td>
						<td><input class="login_username_input" type='text' name='username'></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input class="login_username_input" type='password' name='password'></td>
					</tr>
					<tr>
						<td><input type = 'submit' value = 'login'></td>
					</tr>
				</table>
			
			</form>

			<div class="manage_account_link_box">
				<a href="<?php echo $this->baseUrl."/users/add/$lang";?>"><?php echo gtext("create new account");?></a>
			</div>

			<div class="manage_account_link_box">
				<a href="<?php echo $this->baseUrl."/users/forgot/$lang";?>"><?php echo gtext("request new password");?></a>
			</div>
		</div>
	
	<?php } else { ?>
	
		<div class="login_note">
			Sorry, we are updating the website... it is no possible to log-in, register new accounts or request a new password. You will be able to log-in or create a new account as soon as possible. Thanks!
		</div>
	
	<?php } ?>
