<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">

		<?php
			$tableName = strcmp($this->action,'page') === 0 ? 'wiki' : 'wiki_revisions';
			$displayClass = ($isDeleted) ? 'display_none' : null;
		?>

		<?php if ( strcmp($this->action,'page') === 0 ) { ?>

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang/Main-Page";?>">Wiki</a> &raquo; <?php echo $tree_name;?>
		</div>

		<!--block the page-->
		<?php if ($isadmin) { ?>
		<div class="moderator_box_deleted">
			<?php if ($isBlocked) { ?>
				<?php echo gtext('This wiki page has been blocked'); ?>

				<a id="<?php echo $id_wiki;?>" class="pageunblock_page block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/unlock.png"><?php echo gtext('unblock the wiki page'); ?></a>

			<?php } else {	?>

				<a id="<?php echo $id_wiki;?>" class="pageblock_page block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/lock.png"><?php echo gtext('block the wiki page'); ?></a>

			<?php } ?>

			<?php if ($isDeleted) { ?>
				<div class="wiki_hidden_notice"><?php echo gtext('This wiki page has been deleted'); ?></div>

				<a id="<?php echo $id_wiki;?>" class="pageshow_page_del block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png"><?php echo gtext('restore the wiki page'); ?></a>

			<?php } else {	?>

				<a id="<?php echo $id_wiki;?>" class="pagehide_page_del block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_cancel.png"><?php echo gtext('delete the wiki page'); ?></a>

			<?php } ?>

			<!--view details-->
			<div class="show_hidden_box_ext">
				<div class="md_type">page</div>
				<?php if ($isDeleted) { ?>
				<a class="hidden_message_view_page" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><?php echo gtext("see the page");?></a> |
				<?php } ?>
				<a id="<?php echo $id_wiki;?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
				<div class="moderation_details_box"></div>
			</div>

		</div>
		<?php } ?>

		<div class="notebook_insert_link">
			<div class="view_page_back_button">
				<a title="Insert a new wiki page" href="<?php echo $this->baseUrl."/wiki/insert/$lang";?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/new-60.png"></a>
			</div>

			<?php if (!$isDeleted) { ?>
				<div class="view_page_history_button">
					<a title="talk page" href="<?php echo $this->baseUrl."/wiki/talk/$lang/$id_wiki";?>"><img class="top_left_note_image" src="<?php echo $this->baseUrl;?>/Public/Img/talk-60.png"></a>
				</div>

				<div class="view_page_history_button">
					<a title="history page" href="<?php echo $this->baseUrl."/wiki/history/$lang/$id_wiki";?>"><img class="top_left_note_image" src="<?php echo $this->baseUrl;?>/Public/Img/history-60.png"></a>
				</div>
			<?php } ?>

			<?php if (!$isDeleted and !$isBlocked) { ?>
			<div class="view_page_update_button">
				<form action="<?php echo $this->baseUrl."/wiki/update/$lang";?>" method="POST">
					<input title="edit page" class="update_submit_class" type="image" src="<?php echo $this->baseUrl;?>/Public/Img/edit-60.png" value="xedit">
					<input type="hidden" name="id_wiki" value="<?php echo $id_wiki;?>">
				</form>
			</div>
			<?php } ?>
		</div>

		<?php if (!$isDeleted) { ?>
			<div class="talk_numb_ext_wiki">
				<a href="<?php echo $this->baseUrl."/wiki/talk/$lang/$id_wiki";?>"><?php echo gtext("talk messages");?>: <?php echo $talk_number;?></a>
			</div>
		<?php } ?>
		
		<?php } else if ( strcmp($this->action,'revision') === 0 ) { ?>

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo $tree; ?>
		</div>

		<div class="notebook_view_title">
			<?php echo gtext("Revision of the wiki page");?> <b><?php echo $tree_name;?></b>
		</div>
			
		<div class="notebook_insert_link">
			<a title="Back to the history of the page <?php echo $tree_name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/".$id_wiki.$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
		</div>

		<?php } ?>

		<div class="wiki_external_box <?php echo $displayClass;?>">

			<?php if (!$isDeleted or $isadmin) { ?>
			
				<!--if revision-->
				<?php if (strcmp($this->action,'revision') === 0) { ?>
				<div class="revision_alert">
					<?php echo gtext("This is an old revision of this page, as edited by");?> <b><?php echo $u->getLinkToUserFromId($created_by);?></b> <?php echo gtext('at');?> <b><?php echo smartDate($update_date); ?></b>. <?php echo gtext("It may differ significantly from the");?> <a href="<?php echo $wiki->toWikiPage($id_wiki);?>"><?php echo gtext("Current revision");?></a></a>.
				</div>
				<?php } ?>

				<?php foreach ($table as $page) { ?>
				<div class="wiki_page_title">
					<?php echo $page[$tableName]['title']?>
					<?php if ( strcmp($this->action,'page') === 0 ) { ?>
						<?php if ($isBlocked) { ?>
							<span class="blocked_notice"><?php echo gtext('This wiki page has been blocked'); ?></span>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="wiki_page_content">
					<?php echo decodeWikiText($page[$tableName]['page'])?>
				</div>
				<?php } ?>

			<? } ?>

		</div>

		<?php if ($isDeleted) { ?>
			<div class="revision_alert">
				<?php echo gtext('This wiki page has been deleted'); ?>
			</div>
		<?php } ?>
