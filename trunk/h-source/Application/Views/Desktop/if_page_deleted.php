<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

		<?php if ($isDeleted and $isApproved) { ?>
		
		<div class="notebooks_viewall">
			<div class="revision_alert">
				<div>
					This page has been deleted as requested by:
					<?php foreach ($deletionUsers as $user) { ?>
						<?php echo $u->getLinkToUserFromId($user);?>
					<?php } ?>
				</div>
			</div>
			<div class="deletion_motivations_title">
				With the following motivations:
			</div>
			<div class="deletion_motivations_external">
				<?php foreach ($deletion as $row) { ?>
				<div class="deletion_motivations_iternal">
					<div class="deletion_motivations_iternal_title">
						motivation of <?php echo $u->getLinkToUserFromId($row['deletion']['created_by']);?>: <?php echo getMotivation($row,$this->controller);?>
					</div>
					<div class="deletion_motivations_iternal_message">
						message: <i><?php echo $row['deletion']['message'];?></i>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

		<?php } else if ($isDeleted and !$isApproved) {	?>

		<div class="notebooks_viewall">
			<div class="revision_alert">
				<?php echo gtext('Thanks for your contribution!'); ?><br />
				<?php echo gtext('The device page has to be approved by an administrator of the website'); ?>
			</div>
		</div>
		
		<?php } ?>