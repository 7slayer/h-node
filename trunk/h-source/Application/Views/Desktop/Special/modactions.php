<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/my/home/$lang/$token";?>"><?php echo gtext('panel');?></a> &raquo; list of actions
		</div>
		
		<div class="contrib_explain_box">
			<?php echo $viewTitle; ?>
		</div>
		
		<table class="listTable">
			<tr class="listHead">
				<td class="simpleText">ACTION ID</td>
				<td class="simpleText"><?php echo $user_status;?></td>
				<td class="simpleText">ACTION</td>
				<td class="simpleText">TYPE</td>
				<td class="simpleText">OBJECT ID</td>
				<td class="simpleText text_item_date">DATE</td>
				<?php if (strcmp($this->action,'usersactions') !== 0) { ?>
				<td class="simpleText">NOTE</td>
				<?php } ?>
			</tr>
			<tr>
				<td>&nbsp</td>
				<td>
					<form class="user_action_filter" action="<?php echo $this->currPage."/1/undef";?>">
						<input class="user_action_filter_text" type="text" name="username" value="<?php echo $filterValue;?>" />
						<a title='clear the filter' href='<?php echo $this->currPage."/1/undef";?>'><img src='<?php echo Url::getRoot();?>/Public/Img/Icons/elementary_2_5/clear_filter.png' /></a>
						<input class="user_action_filter_submit" type='image' title='filter' src='<?php echo $this->baseUrl?>/Public/Img/Icons/elementary_2_5/find.png' value='trova'>
					</form>
				</td>
				<td>&nbsp</td>
				<td>&nbsp</td>
				<td>&nbsp</td>
				<td>&nbsp</td>
				<?php if (strcmp($this->action,'usersactions') !== 0) { ?>
				<td>&nbsp</td>
				<?php } ?>
			</tr>
			<?php foreach ($table as $row) { ?>
			<tr class="listRow">
				<td class="simpleText">
					<span class='textItem'><a href="<?php echo goToModeratedItem($row['history']);?>"><?php echo $row['history']['id_history'];?></a></span>
				</td>
				<td class="simpleText">
					<span class='textItem'><?php echo $u->getLinkToUserFromId($row['history']['created_by']);?></span>
				</td>
				<td class="simpleText">
					<span class='textItem'><?php echo HistoryController::$actionTable[$row['history']['action']];?></span>
				</td>
				<td class="type_column">
					<span class='textItem'><?php echo HistoryController::$typeTable[$row['history']['type']];?></span>
				</td>
				<td class="simpleText">
					<span class='textItem'><?php echo $row['history']['id'];?></span>
				</td>
				<td class="simpleText text_item_date">
					<span class='textItem'><?php echo smartDate($row['history']['creation_date']);?></span>
				</td>
				<?php if (strcmp($this->action,'usersactions') !== 0) { ?>
				<td class="simpleText">
					<span class='textItem'><?php echo $row['history']['message'];?></span>
				</td>
				<?php } ?>
			</tr>
			<?php } ?>
		</table>

		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		
	</div>
