<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="footer">		
		<div class="copyright_notice_box">
			The <a href="<?php echo $this->baseUrl."/project/index/$lang";?>"><?php echo Website::$projectName;?></a> Project
		</div>
		
		<div class="footer_credits_box">
			<a href="<?php echo $this->baseUrl."/credits/index/$lang";?>"><?php echo gtext("credits");?></a>
		</div>
		
		<div class="footer_credits_box">
			<a href="<?php echo $this->baseUrl."/contact/index/$lang";?>"><?php echo gtext("contact us");?></a>
		</div>
	</div> <!--fine footer-->
	
	<div style="padding:5px 0px;font-size:12px;"><a href="<?php echo $this->baseUrl;?>/static/licenses.html" rel="jslicense">JavaScript license information</a>
	</div>
</div> <!--fine container-->

</body>
</html> 
