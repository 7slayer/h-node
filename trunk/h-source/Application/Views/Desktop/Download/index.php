<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("Download");?>
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				<?php echo gtext("Download the h-node hardware database in xml format");?>:
			</div>
			
			<div class="credits_item_description">
				<?php echo gtext("You can download all the h-node database in one unique xml file in order to parse its contents by means of some proper script (for example a Python or Perl or PHP script)");?>
			
				<div class="download_table">
					<table width="95%">
						<tr>
							<td><?php echo gtext("Download the xml file of all the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/all/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>notebooks</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/notebooks/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>wifi cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/wifi/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>video cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/videocards/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>printers</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/printers/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>scanners</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/scanners/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>3G cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/threegcards/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>sound cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/soundcards/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>webcams</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/webcams/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>bluetooth devices</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/bluetooth/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>acquisition cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/acquisitioncards/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>fingerprint readers</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/fingerprintreaders/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>ethernet cards</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/ethernetcards/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>SD card readers</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/sdcardreaders/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>modems</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/modems/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>RAID adapters</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/raidadapters/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
						<tr>
							<td><?php echo gtext("Download the xml file of all the <b>Host Controllers</b> in the database");?></td>
							<td><a href="<?php echo $this->baseUrl."/download/hostcontrollers/$lang";?>"><img src="<?php echo $this->baseUrl?>/Public/Img/H2O/download.png"></a></td>
						</tr>
					</table>
				</div>
			</div>
			
		</div>
		
	</div>
